import React, { useEffect, useRef } from "react";
import cookie from "react-cookies";
function Paypal(props) {
  const paypal = useRef();
  useEffect(() => {
    window.paypal
      .Buttons({
        createOrder: (data, actions, err) => {
          return actions.order.create({
            intent: "CAPTURE",
            purchase_units: [
              {
                description: cookie.load("carts")[0].name,
                amount: {
                  currency_code: "NZD",
                  value: props.subtotal,
                },
              },
            ],
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          console.log(order);
          props.submitOrder(order);
        },
        onError: (err) => {
          console.log(err);
        },
      })
      .render(paypal.current);
  });

  return <div ref={paypal}></div>;
}
export default Paypal;
