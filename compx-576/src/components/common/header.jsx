import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import cookie from "react-cookies";
import "../../assets/css/menu.css";
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  Button,
  Modal,
  Table,
} from "react-bootstrap";
import Axios from "axios";
import MD5 from "crypto-js/md5";
import { LinkContainer } from "react-router-bootstrap";
import brandImg from "../../assets/images/mobile_outlet4-small.png";
import { PersonCircle, Cart4 } from "react-bootstrap-icons";
import "../../assets/css/trolley.css";
class Header extends Component {
  state = {
    show: false,
    userID: "",
    userName: "",
    num: 0,
    price: 0,
    carts: [],
    showTopTrolley: false,
  };
  userEmail = React.createRef();
  userPwd = React.createRef();

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = () => {
    this.setState({ show: true });
  };
  // componentWillMount() {
  //   this.state = { userID: cookie.load("userID") };
  // }
  onLogin = () => {
    Axios.post("http://75.101.153.156:3001/user-login", {
      email: this.userEmail.current.value,
      pwd: MD5(this.userPwd.current.value).toString(),
    }).then((Response) => {
      if (Response.data.length) {
        this.setState({ userID: Response.data[0].id });
        this.setState({ userName: Response.data[0].first_name });
        cookie.save("userID", this.state.userID, { path: "/" });
        cookie.save("userName", this.state.userName, { path: "/" });
        this.handleClose();
        alert("Welcome, " + this.state.userName);
      } else {
        alert(
          "Email or password wrong. Please check again or try your email address. "
        );
      }
    });
  };
  onLogout = () => {
    cookie.remove("userID", { path: "/" });
    cookie.remove("userName", { path: "/" });
    this.setState({ userID: "" });
    this.setState({ userName: "" });
  };

  trolleyData = () => {};

  hideTopModal = () => {
    this.setState({ showTopTrolley: false });
  };
  showTopTrolley = () => {
    this.setState({ showTopTrolley: true });
  };

  modalTopCloseHandle = (val) => {
    this.setState({ showTopTrolley: val });
  };
  saveTrolley = (products) => {
    if (products !== []) {
      this.setState({ carts: products });
    }
  };
  componentDidMount() {
    // cookie.save("carts", this.state.carts);
  }
  componentWillMount() {
    cookie.save("carts", this.state.carts);
  }

  render() {
    return (
      <>
        <section>
          <div className="header-top">
            <div className="brand">
              <NavLink to="/">
                <img src={brandImg} alt="mobile outlet" />
              </NavLink>
            </div>
            <div className="trolley-box">
              <Button
                className="trolley-btn"
                onMouseEnter={this.showTopTrolley}
              >
                {" "}
                <Cart4 /> Trolley
              </Button>
              {
                <Modal
                  show={this.state.showTopTrolley}
                  onHide={this.hideTopModal}
                >
                  <Modal.Header closeButton>
                    <Modal.Title className="trolley-title-top">
                      <Cart4 /> Trolley
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body className>
                    <Table borderless size="sm">
                      {cookie.load("carts").length === 0 ? (
                        <tbody>
                          <tr>
                            <td>Empyt... Please add some products.</td>
                          </tr>
                        </tbody>
                      ) : (
                        <tbody>
                          {cookie.load("carts").map((pro, index) => {
                            return (
                              <>
                                <tr key={index} className="trolley-item">
                                  <td>
                                    {pro.name} {pro.color} {pro.storages}(
                                    {pro.grade} Grade)
                                  </td>
                                  <td>
                                    <strong>x {pro.count}</strong>
                                  </td>
                                </tr>
                                <tr className="trolley-item-price">
                                  <td>${pro.total}</td>
                                </tr>
                              </>
                            );
                          })}

                          <tr className="trolley-item-price">
                            <td>
                              Subtotal:{" "}
                              <u>
                                $
                                {cookie.load("carts").length === 0
                                  ? ""
                                  : cookie.load("carts").slice(-1)[0].subtotal}
                              </u>
                            </td>
                          </tr>
                        </tbody>
                      )}
                    </Table>
                  </Modal.Body>
                  <Modal.Footer>
                    <Link to="/checkout">
                      <Button variant="success">Check Out</Button>
                    </Link>
                    <Button variant="danger" onClick={this.hideTopModal}>
                      Close
                    </Button>
                  </Modal.Footer>
                </Modal>
              }
            </div>
          </div>
          <Navbar
            collapseOnSelect
            expand="lg"
            bg="dark"
            variant="dark"
            sticky="top"
          >
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />

            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <NavDropdown title="iPhone" id="collasible-nav-dropdown">
                  <LinkContainer
                    to="/iphone/iphone-x"
                    activeClassName="navbar-active-c"
                  >
                    <NavDropdown.Item>iphone X</NavDropdown.Item>
                  </LinkContainer>
                  <NavDropdown.Divider />
                  <LinkContainer
                    to="/iphone/iphone-11"
                    activeClassName="navbar-active-c"
                  >
                    <NavDropdown.Item>iphone 11</NavDropdown.Item>
                  </LinkContainer>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.4">
                    Separated link
                  </NavDropdown.Item>
                </NavDropdown>
                <NavDropdown
                  title="Sumsung Galaxy"
                  id="collasible-nav-dropdown"
                >
                  <LinkContainer
                    to="/sumsung/note-8"
                    activeClassName="navbar-active-c"
                  >
                    <NavDropdown.Item>Galaxy Note 8</NavDropdown.Item>
                  </LinkContainer>
                  <NavDropdown.Divider />
                  <LinkContainer
                    to="/sumsung/galaxy-s10"
                    activeClassName="navbar-active-c"
                  >
                    <NavDropdown.Item>Galaxy S10</NavDropdown.Item>
                  </LinkContainer>
                </NavDropdown>
                <LinkContainer
                  exact
                  to="/about-us"
                  activeClassName="navbar-active-c"
                >
                  <Nav.Link>About Us</Nav.Link>
                </LinkContainer>
                <LinkContainer
                  exact
                  to="/contact-us"
                  activeClassName="navbar-active-c"
                >
                  <Nav.Link>Contact Us</Nav.Link>
                </LinkContainer>
              </Nav>
            </Navbar.Collapse>
            <div className="login-box">
              {cookie.load("userID") ? (
                <>
                  <Link to="/myaccount" className="logged-show">
                    <PersonCircle></PersonCircle>
                    <span>Hello, {cookie.load("userName")}</span>
                  </Link>
                  <Button
                    onClick={this.onLogout}
                    variant="secondary"
                    className="logout-btn"
                  >
                    Logout
                  </Button>
                </>
              ) : (
                <a href="#">
                  <PersonCircle onClick={this.handleShow}></PersonCircle>
                </a>
              )}

              <Modal show={this.state.show} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div className="login-box-content">
                    <Form.Group controlId="formBasicEmail">
                      <Form.Label>Email address</Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Enter email"
                        ref={this.userEmail}
                      />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                      <Form.Label>Password</Form.Label>
                      <Form.Control
                        type="password"
                        placeholder="Password"
                        ref={this.userPwd}
                      />
                    </Form.Group>
                    <Button
                      variant="primary"
                      type="submit"
                      className="login-box-btn btn-warning btn-primary-hover"
                      onClick={this.onLogin}
                    >
                      Login
                    </Button>
                    <div className="login-box-create-account">
                      <Link to="/register" onClick={this.handleClose}>
                          Create an account
                      </Link>
                    </div>
                  </div>
                </Modal.Body>
              </Modal>
            </div>
          </Navbar>
        </section>
      </>
    );
  }
}

export default Header;
