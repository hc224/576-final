import React from "react";
import cookie from "react-cookies";
function FtechCookies(props) {
  return cookie.load("carts").map((product, index) => {
    return (
      <>
        <tbody>
          <tr className="trolly-top-item">
            <td>
              {product.name} {product.color} {product.storages} ({product.grade}{" "}
              Grade)
            </td>
            <td>
              <strong>x {product.count}</strong>{" "}
            </td>
          </tr>
          <tr className="trolley-top-item-price">
            <td>${product.price}</td>
          </tr>
        </tbody>
      </>
    );
  });
}
export default FtechCookies;
