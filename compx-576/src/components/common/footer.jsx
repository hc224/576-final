import React, { Component } from "react";
import { Container } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import {
  TelephoneFill,
  EnvelopeFill,
  GeoAltFill,
  Building,
  ShieldLockFill,
  InfoCircleFill,
} from "react-bootstrap-icons";
import footerImg from "../../assets/images/mobile_outlet_black.jpg";
import { NavLink } from "react-router-dom";
class Footer extends Component {
  render() {
    return (
      <footer className="navbar-fixed-bottom">
        <Container fixed="bottom" className="footer" fluid>
          <Row>
            <Col sm={12} md={5}>
              <h5 className="color-o1">CONTACT</h5>
              <div>
                <ul className="list-unstyled footer-list">
                  <li>
                    <TelephoneFill></TelephoneFill>
                    <span>88-8888-888</span>
                  </li>
                  <li>
                    <EnvelopeFill></EnvelopeFill>
                    <span>support@mobileoutlet.co.nz</span>
                  </li>
                  <li>
                    <GeoAltFill></GeoAltFill>
                    <span>2B Empire Road, Epsom, Auckland</span>
                  </li>
                </ul>
              </div>
            </Col>
            <Col sm={12} md={4}>
              <h5 className="color-o1">QUICK LINK</h5>
              <div>
                <ul className="list-unstyled footer-list">
                  <li>
                    <Building></Building>
                    <NavLink to="/about-us">
                      <span>About Us</span>
                    </NavLink>
                  </li>
                  <li>
                    <ShieldLockFill></ShieldLockFill>
                    <span>Privacy Policy</span>
                  </li>
                  <li>
                    <InfoCircleFill></InfoCircleFill>
                    <span>Warranty & Terms & Coditions</span>
                  </li>
                </ul>
              </div>
            </Col>
            <Col sm={12} md={3}>
              <h5 className="color-o1">MOBILE OUTLET</h5>
              <div>
                <img src={footerImg} className="footer-img" alt='mobileoutlet-footer' />
              </div>
            </Col>
            <Col sm={12} md={12} className="copyright">
              © 2020 Mobile Outlet. All Rights Reserved.
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}

export default Footer;
