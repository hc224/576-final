import React, { Component } from "react";
import {
  Button,
  Modal,
  Table,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { CheckCircleFill } from "react-bootstrap-icons";
import "../../assets/css/trolley.css";
import cookie from "react-cookies";
class Trolley extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carts: cookie.load("carts"),
      //   (cookie.load('carts').length===0 ? []:cookie.load('carts')),
      currentNum: -1,
    };
  }

  addToCart = (sku) => {
    // if(this.state.carts.length!=0){
    let index = this.state.carts.findIndex((item) => item.sku === sku); //if there already exist the item(sku), count++
    if (index !== -1) {
      let cart = this.state.carts[index];
      cart.count++;
      cart.total += cart.price;
      cart.subtotal += cart.price;
      this.setState({ currentNum: cart.count });
      this.state.carts.splice(index, 1);
      this.setState({
        carts: [...this.state.carts, cart],
      });
      this.setState({ subtotal: this.state.carts.subtotal + cart.price });
    } else {
      //if there is new item(sku)
      if (this.state.carts.length === 0) {
        //from first product (no subtotal)
        let cart = {
          count: 1,
          sku: this.props.sku,
          name: this.props.name,
          color: this.props.color,
          storages: this.props.storages,
          grade: this.props.grade,
          price: this.props.price,
          total: this.props.price,
          subtotal: this.props.price,
        };
        this.setState({
          carts: [...this.state.carts, cart],
        });

      } else {
        let cart = {
          count: 1,
          sku: this.props.sku,
          name: this.props.name,
          color: this.props.color,
          storages: this.props.storages,
          grade: this.props.grade,
          price: this.props.price,
          total: this.props.price,
          subtotal:
            parseInt(
              this.state.carts.map((pro, i) => {
                pro.sbutotal += pro.sbutotal;
                return pro.subtotal;
              })
            ) + this.props.price,
        };
        this.setState({
          carts: [...this.state.carts, cart],
        });
      }
    }
    // }
  };


  hideModal = () => {
    this.props.modalHandle(false, this.state.currentNum);
  };

  componentWillMount() {
    // cookie.save('carts',[])
  }
  componentDidMount() {
    // cookie.save('carts',[]);
    this.props.onRef(this);
    //  this.addToCart(this.props.sku);
  }

  render() {
    cookie.save("carts", JSON.stringify(this.state.carts));
    // this.setState({price:this.props.price});

    return (
      <>
        <Modal show={this.props.visible} onHide={this.hideModal}>
          <Modal.Header closeButton>
            <Modal.Title className="trolley-title">
              <CheckCircleFill />
              Product added to your cart.
              <div className="trolley-subtitle">
                You've added the
                <u>
                  {" "}
                  {this.props.name} {this.props.color} {this.props.storages}{" "}
                  {this.props.grade + " Grade"}
                </u>{" "}
                to your cart
              </div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className>
            <Table borderless size="sm">
              {this.state.carts.length === 0
                ? ""
                : 
                (
                    <>
                     <tbody className="trolley-table">
                         {
                this.state.carts.map((product, index) => {
                    return (
                     <>
                        <tr className="trolley-item">
                          <td>
                            {product.name} {product.color} {product.storages} (
                            {product.grade} Grade)
                          </td>
                          <td>
                            <strong>x {product.count}</strong>{" "}
                          </td>
                        </tr>
                        <tr className="trolley-item-price">
                          <td>${product.total}</td>
                        </tr>
                        </>
               
                    );
                  })
                }
                  <tr className="trolley-item-price">
                  <td>
                    
                      Subtotal: <u>$
                      {cookie.load("carts").length === 0 ? '':cookie.load("carts").slice(-1)[0].subtotal}
                    </u>
                  </td>
                </tr>
                
                 </tbody>
                </>
                )
                  
                  
                  }
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <Link to='/checkout'><Button variant="success" onClick={this.hideModal}>
              Check Out
            </Button></Link> 
            <Button variant="danger" onClick={this.hideModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default Trolley;
