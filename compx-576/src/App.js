import React from "react";

import Header from "./components/common/header";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import NotFound from "./pages/404";

import Home from "./pages/home";

import ContactUS from "./pages/contact-us";

import About from './pages/about-us';
import Footer from './components/common/footer';
import './assets/css/menu.css';
import './assets/css/colors.css';
import './assets/css/home.css';
import './assets/css/font.css';
import './assets/css/main.css';
import "./assets/css/register.css";
import "./assets/css/myaccount.css";
import "./assets/css/thankyou.css";
import ProductDescription from "./pages/product-description";
import Checkout from "./pages/checkout";
import Register from "./pages/register";
import Myaccount from "./pages/myaccount";
import ThankYou from "./pages/thank-you";

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        

        <Switch>
          <Route exact path="/" component={Home}></Route>
         
          
          {/* <Route exact path="/iphone/" component={Product}></Route> */}
          
          <Route exact path="/contact-us" component={ContactUS}></Route>
          <Route exact path="/about-us" component={About}></Route>
          <Route  path="/product-description" component={ProductDescription}></Route>
          <Route exact path="/register" component={Register}></Route>
          <Route exact path="/checkout" component={Checkout}></Route>
          <Route exact path="/myaccount" component={Myaccount}></Route>
          <Route exact path="/thank-you" component={ThankYou}></Route>
          {/* <Route exact path="/input" component={ContentInput}></Route> */}
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
