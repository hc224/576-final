import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
  } from "react-bootstrap";
import cookie from "react-cookies";
class ThankYou extends Component {
    state = { 

     }
     componentDidMount(){
        cookie.remove('carts')
     }
    render() {
        return (
            <>
<Container>
    <Row className="justify-content-center">
       <Col xs="10" md="8" lg="8">
        <div className="thankyou-title">
            <h4>Thank you for your order!</h4>
        </div>
        </Col>
        
    </Row>
</Container>
            </>

        );
    }
}

export default ThankYou;