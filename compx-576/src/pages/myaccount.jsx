import React, { Component } from "react";
import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import Axios from "axios";
import MD5 from 'crypto-js/md5';
import { Person, CartCheck, GeoAlt, BoxArrowLeft } from "react-bootstrap-icons";
class myaccount extends Component {
  constructor() {
    super();
    this.state = {
      toggleState: 1,
      firstName:'',
      lastName:'',
      email:''
    };
  }

  toggleTab = (e) => {
    this.setState({ toggleState: e });
    this.getData();
  };

  saveUserDetail = () => {
    Axios.post("http://chvast.com:3001/register", {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: MD5(this.state.password).toString(),
    }).then(() => {
      alert('save success.');
    });
  };
getUserDetail=()=>{
  Axios.post('http://chvast.com:3001/get-user-info',{
    userID:1
  }).then(()=>{
   alert('s');
  });
}
getUserAddress=()=>{
  Axios.post('http://chvast.com:3001/get-user-address',{
    userID:1
  }).then(()=>{
   alert('s');
  });
}
getUserOrder=()=>{
  Axios.post('http://chvast.com:3001/get-user-order',{
    userID:1
  }).then(()=>{
   alert('s');
  });
}
goHome=()=>{

}

 getData=()=>{
switch(this.state.toggleState){
  case 1:
    this.getUserDetail();
    break;
  case 2:
    this.getUserOrder();
    break;
  case 3:
      this.getUserDetail();
    break;
  case 4:
    this.goHome();
    break;
}


}
  render() {
    return (
      <>
        <Container>
          <Row>
            <Col>
              <h4 className="myaccount-title">My Account</h4>
            </Col>
          </Row>
          <Row>
            <Col xs="12" md="4">
              <div className="myaccount-nav">
                <div
                  className={this.state.toggleState === 1 ? "active-tabs" : " "}
                  onClick={this.toggleTab.bind(this, 1)}
                >
                  <Person></Person>Account Details
                </div>
                <div
                  className={this.state.toggleState === 2 ? "active-tabs" : " "}
                  onClick={this.toggleTab.bind(this, 2)}
                >
                  <CartCheck></CartCheck>Orders
                </div>
                <div
                  className={this.state.toggleState === 3 ? "active-tabs" : " "}
                  onClick={this.toggleTab.bind(this, 3)}
                >
                  <GeoAlt></GeoAlt>Address
                </div>
                <div
                  className={this.state.toggleState === 4 ? "active-tabs" : " "}
                  onClick={this.toggleTab.bind(this, 4)}
                >
                  <BoxArrowLeft></BoxArrowLeft>Logout
                </div>
              </div>
            </Col>



            <Col xs="12" md="8">
              <div className="myaccount-content">

                      {/* ------------------------------------------------------------------------- */}
               
                <div
                  id="1"
                  className={
                    this.state.toggleState === 1
                      ? "active-content"
                      : "nagative-content"
                  }
                >
                  <div className="myaccount-details myaccount-content-form">
                    <h4>Account Details</h4>
                    <Form>
                      <Row>
                        <Form.Group as={Col} controlId="formGridFirstName">
                          <Form.Label>First Name</Form.Label>
                          <Form.Control required placeholder="First Name" />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridLastName">
                          <Form.Label>Last Name</Form.Label>
                          <Form.Control required placeholder="Last Name" />
                        </Form.Group>
                      </Row>

                      <Form.Group controlId="formGridEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                          required
                          type="Email"
                          placeholder="Enter Email"
                        />
                      </Form.Group>
                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>New Password</Form.Label>
                        <Form.Control type="password" />
                      </Form.Group>
                      <Button
                        type="submit"
                        className="btn btn-warning btn-primary-hover form-btn"
                        onClick={this.saveUserDetail}
                      >
                        Save
                      </Button>
                    </Form>
                  </div>
                </div>

                {/* ------------------------------------------------------------------------- */}

                <div
                  id="2"
                  className={
                    this.state.toggleState === 2
                      ? "active-content"
                      : "nagative-content"
                  }
                >
                  <div className="myaccount-orders myaccount-content-form">
                    <h4>Orders</h4>
                    <Table striped bordered hover>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Product Name</th>
                          <th>Date</th>
                          <th>Total</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>iPhone 11 64GB Purple</td>
                          <td>22 Mar, 2021</td>
                          <td>$899</td>
                          <td>Processing</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>iPhone 11 64GB Purple</td>
                          <td>22 Mar, 2021</td>
                          <td>$899</td>
                          <td>Processing</td>
                        </tr>
                      </tbody>
                    </Table>
                  </div>
                </div>
      {/* ------------------------------------------------------------------------- */}
                <div
                  id="3"
                  className={
                    this.state.toggleState === 3
                      ? "active-content"
                      : "nagative-content"
                  }
                >
                  <div className="myaccount-orders myaccount-content-form">
                    <h4>Address</h4>
                    <Form>
                      <Form.Group controlId="formGridEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                      </Form.Group>

                      <Form.Group controlId="formGridAddress1">
                        <Form.Label>Address</Form.Label>
                        <Form.Control placeholder="1234 Main St" />
                      </Form.Group>

                      <Form.Group controlId="formGridAddress2">
                        <Form.Label>Address 2</Form.Label>
                        <Form.Control placeholder="Apartment, studio, or floor" />
                      </Form.Group>

                      <Form.Row>
                        <Form.Group as={Col} controlId="formGridCity">
                          <Form.Label>City</Form.Label>
                          <Form.Control />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridZip">
                          <Form.Label>Postcode/Zip</Form.Label>
                          <Form.Control />
                        </Form.Group>
                      </Form.Row>

                      <Button
                        variant="primary"
                        type="submit"
                        className="btn btn-warning btn-primary-hover form-btn"
                      >
                        Save
                      </Button>
                    </Form>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default myaccount;
