import React, { Component } from "react";
import { Container } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { Link } from "react-router-dom";
import {
  Col,
  Row,
  Breadcrumb,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import Axios from "axios";
import { MenuButtonWide, Search, BookmarkHeart } from "react-bootstrap-icons";
import image2 from "../assets/images/iphone 12.jpg";
import image3 from "../assets/images/accessories.jpg";
import image4 from "../assets/images/iphone-x-vs-galaxy-s9 3.jpg";
import image5 from "../assets/images/s10 5g 2 600.jpg";

class Home extends Component {
  state = {
    products: [],
  };

  componentDidMount() {
    this.getProducts();
  }

  getProducts = () => {
    Axios.get("http://chvast.com:3001/get-home-products").then((Response) => {
      this.setState({ products: Response.data });
    });
  };

  render() {
    return (
      <>
        <Breadcrumb>
          <LinkContainer to="/">
            <Breadcrumb.Item>Home</Breadcrumb.Item>
          </LinkContainer>

          <Breadcrumb.Item active>iPhone</Breadcrumb.Item>
        </Breadcrumb>

        <section>
          <Container fluid="xl">
            <Row>
              <Col md={4} lg={3}>
                <div className="drop-right">
                  <div className="drop-header">
                    <MenuButtonWide></MenuButtonWide>
                    <span>Browse Categories</span>
                  </div>
                  <DropdownButton
                    id="right"
                    drop="right"
                    title="iPhone"
                    bsPrefix="right-drop-button"
                  >
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Separated link</Dropdown.Item>
                  </DropdownButton>
                  <DropdownButton
                    id="right"
                    drop="right"
                    title="Sumsung Galaxy"
                    bsPrefix="right-drop-button"
                  >
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Separated link</Dropdown.Item>
                  </DropdownButton>
                  <DropdownButton
                    id="right"
                    drop="right"
                    title="iPad"
                    bsPrefix="right-drop-button"
                  >
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Separated link</Dropdown.Item>
                  </DropdownButton>
                  <DropdownButton
                    id="right"
                    drop="right"
                    title="Macbook"
                    bsPrefix="right-drop-button"
                  >
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Separated link</Dropdown.Item>
                  </DropdownButton>
                  <DropdownButton
                    id="right"
                    drop="right"
                    title="Accessories"
                    bsPrefix="right-drop-button"
                  >
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Separated link</Dropdown.Item>
                  </DropdownButton>
                  <DropdownButton
                    id="right"
                    drop="right"
                    title="Loudspeaker"
                    bsPrefix="right-drop-button"
                  >
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Separated link</Dropdown.Item>
                  </DropdownButton>
                  <DropdownButton
                    id="right"
                    drop="right"
                    title="Wireless Charger"
                    bsPrefix="right-drop-button"
                  >
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Separated link</Dropdown.Item>
                  </DropdownButton>
                </div>
              </Col>
              <Col md={8} lg={7}>
                <div className="top-info">
                  <img src={image4} alt="image4" />
                </div>
              </Col>

              <Col md={12} lg={2}>
                <div className="right-box">
                  <Link to="/iphone">
                    <img src={image2} alt="image2" />
                  </Link>
                  <Link to="/accessories">
                    <img className="accessories-pic" src={image3} alt="img3" />
                  </Link>
                  <Link to="/samsung">
                    <img src={image5} alt="img5" />
                  </Link>
                </div>
              </Col>
            </Row>

            <div className="divider"></div>

            <Row className="product-group">
              {this.state.products.map((product, index) => {
                return (
                  <Col xs={6} md={3} className="col-5-c product-card">
                    <div className="product-individual">
                      <Link
                        to={{
                          pathname:
                            "/product-description/" +
                            product.product_name +
                            product.id,
                          state: { skuID: product.id },
                        }}
                      >
                        <img src={product.pic_src} alt="product-pic" />
                      </Link>
                      <div className="product-intro">
                        <Link to="/product" className="product-series">
                          {product.product_name}
                        </Link>
                        <span> ({product.grade} grade)</span>
                        <div className="product-detail">
                          <Link
                            to={{
                              pathname:
                                "/product-description/" +
                                product.product_name +
                                product.id,
                              state: { skuID: product.id },
                            }}
                          >
                            <a>
                              <div>
                                {product.storages} {product.color}
                              </div>
                            </a>
                          </Link>
                        </div>
                        <div className="product-price">
                          <span className="regular-price">
                            ${product.regular_price}
                          </span>
                          <span className="sale-price">
                            ${product.sale_price}
                          </span>
                        </div>
                        <Link
                          to={{
                            pathname:
                              "/product-description/" +
                              product.product_name +
                              product.id,
                            state: { skuID: product.id },
                          }}
                        >
                          <button className="learn-more-btn btn btn-warning btn-primary-hover">
                            Learn More
                          </button>
                        </Link>
                      </div>
                      <ul className="product-slider list-unstyled">
                        <li className="quick-view">
                          <button className="btn-primary-hover">
                            <Search></Search>
                          </button>
                        </li>
                        <li className="wishlist">
                          <button className="btn-primary-hover">
                            <BookmarkHeart></BookmarkHeart>
                          </button>
                        </li>
                      </ul>
                    </div>
                  </Col>
                );
              })}
            </Row>
          </Container>
        </section>
      </>
    );
  }
}

export default Home;
