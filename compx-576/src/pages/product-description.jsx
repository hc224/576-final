import React, { Component } from "react";
import {
  Col,
  Row,
  Breadcrumb,
  Container
} from "react-bootstrap";
import Axios from "axios";
import { LinkContainer } from "react-router-bootstrap";
import { StarFill, Cart2 } from "react-bootstrap-icons";
import laybuy from "../assets/images/laybuy.png";
import genoapay from "../assets/images/genoapay.png";
import afterpay from "../assets/images/afterpay.png";
import "../assets/css/product-detail.css";
import image5 from "../assets/images/s10 5g 2 600.jpg";
import Trolley from "../components/common/trolley";
class ProductDescription extends Component {
  state = {
    productDetail: [],
    regularPrice: "",
    salePrice: "",
    productDesc: "Not Set",
    showTrolley: false,
    currentNum: 0,
  };

  getSKU = () => {
    Axios.post("http://chvast.com:3001/get-sku-desc", {
      skuID: this.props.location.state.skuID,
    }).then((Response) => {
      this.setState({ productDetail: Response.data[0] });
      this.setState({ regularPrice: this.state.productDetail.regular_price });
      this.setState({ salePrice: this.state.productDetail.sale_price });
    });
  };
  getProductDesc = () => {
    Axios.post("http://chvast.com:3001/get-product-desc", {
      skuID: this.props.location.state.skuID,
    }).then((Response) => {
      if(Response.data.length===0){
        console.log('NOT SET DESC!!!');
      }else{
        this.setState({ productDesc: Response.data[0].description });
        // let reg = new RegExp('"', "g");
        // this.setState({ productDesc: this.state.productDesc.replace(reg, "") });
        // d = d.replace(/\"/g, "");
      }
     

    });
  };

  componentDidMount() {
    this.getSKU();
    this.getProductDesc();
  }
  addTrolley = () => {
    this.setState({ showTrolley: true });
    this.child.addToCart(this.props.location.state.skuID);
  };
  closeTrolley = (val, num) => {
    this.setState({ showTrolley: val });

    this.setState({ currentNum: num });
  };

  onRef = (ref) => {
    this.child = ref;
  };

  render() {
    return (
      <>
        {/* {this.state.showTrolley && */}
        <Trolley
          onRef={this.onRef}
          modalHandle={this.closeTrolley.bind(this)}
          visible={this.state.showTrolley}
          sku={this.props.location.state.skuID}
          name={this.state.productDetail.product_name}
          storages={this.state.productDetail.storages}
          color={this.state.productDetail.color}
          grade={this.state.productDetail.grade}
          price={this.state.productDetail.sale_price}
        />
        {/* } */}
        <Breadcrumb>
          <LinkContainer to="/">
            <Breadcrumb.Item>Home</Breadcrumb.Item>
          </LinkContainer>

          <Breadcrumb.Item active>iPhone</Breadcrumb.Item>
        </Breadcrumb>
        <Container fluid="xl">
          <Row>
            <Col xs="12" md="5" lg="4">
              <div className="detail-pic">
                <img src={this.state.productDetail.pic_src} alt="" />
              </div>
            </Col>
            <Col xs="12" md="7" lg="6">
              <div className="detail-content">
                <div className="detail-title">
                  <h6 className="title-grade">
                    {this.state.productDetail.grade} Grade
                  </h6>
                  <h4 className="title-series">
                    {this.state.productDetail.product_name}
                  </h4>
                  <h4 className="title-info">
                    {this.state.productDetail.storages}&nbsp;
                    {this.state.productDetail.color}
                  </h4>

                  <h6>12 Months Warranty</h6>
                </div>

                <div className="detail-price">
                  <span className="regular-price">
                    ${this.state.regularPrice}
                  </span>
                  <span className="sale-price">${this.state.salePrice}</span>
                  <span className="price-save">
                    save&nbsp;
                    {parseInt(
                      ((this.state.regularPrice - this.state.salePrice) /
                        this.state.regularPrice) *
                        100
                    )}
                    %
                  </span>
                  <div>{this.state.productDetail.stock} in Stock</div>
                </div>
                <Row>
                  <Col xs="4" className="payments">
                    <img src={laybuy} alt="Laybuy" />
                    <div>
                      <strong>6 weekly</strong> interest-free from $
                      {(this.state.regularPrice / 6).toFixed(2)}
                    </div>
                  </Col>
                  <Col xs="4" className="payments">
                    <img src={genoapay} alt="Genoapay" />
                    <div>
                      <strong>10 weekly</strong> interest-free from $
                      {(this.state.regularPrice / 10).toFixed(2)}
                    </div>
                  </Col>
                  <Col xs="4" className="payments">
                    <img src={afterpay} alt="Afterpay" />
                    <div>
                      <strong>4 fortnightly</strong> interest-free from $
                      {(this.state.regularPrice / 4).toFixed(2)}
                    </div>
                  </Col>
                </Row>
                <div>
                  <button
                    className="add-cart-button btn btn-warning btn-primary-hover"
                    onClick={this.addTrolley}
                  >
                    <Cart2></Cart2> Add to Cart{" "}
                    {this.state.currentNum > 0
                      ? "(" + this.state.currentNum + ")"
                      : ""}
                  </button>
                </div>
              </div>
              <div className="detail-right-box">
                <div className="reviews">
                  <StarFill></StarFill>
                  <StarFill></StarFill>
                  <StarFill></StarFill>
                  <StarFill></StarFill>
                  <StarFill></StarFill>
                  <span> (3 reviews)</span>
                </div>
                <div className="short-intro">
                  <ul className="list-unstyled">
                    <li>
                      <b>Cellular Connectivity: </b>4G
                    </li>
                    <li>
                      <b>Screen Size: </b>6.1 inches
                    </li>
                    <li>
                      <b>Camera: </b>Dual 12MP
                    </li>
                    <li>
                      <b>Chip: </b>A14 Bionic chip
                    </li>
                  </ul>
                </div>
              </div>
            </Col>
            <Col lg="2">
              <div className="side-product">
                <img src={image5} alt="" />
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </Col>
          </Row>
          <Row>
            <Col xs="12" className="detail-include"></Col>
          </Row>
          <Row>
            <Col xs="12" className="detail-description">
              <div className="description-title">DESCRIPTION</div>
              <div
                className="description-content"
                dangerouslySetInnerHTML={{ __html: this.state.productDesc }}
              ></div>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default ProductDescription;
