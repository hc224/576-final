import React, { Component } from "react";
import "../assets/css/checkout.css";
import cookie from "react-cookies";
import {
  Col,
  Row,
  Container,
  Form,
  Button,
} from "react-bootstrap";
import Paypal from '../components/common/Paypal'
import Axios from "axios";
import paypal from '../assets/images/paypal-muti.png';
class Checkout extends Component {
state={
subtotal:-1,
orderNo:-1,
orderItems:[],
payment:'bank',
orderStatus:'on-hold'
}

  firstName = React.createRef();
  lastName = React.createRef();
  email = React.createRef();
  address1 = React.createRef();
  address2 = React.createRef();
  city = React.createRef();
  zip = React.createRef();
  shippingFee = 0;
componentWillMount(){
  this.setState({subtotal:cookie.load("carts").length === 0 ? -1:cookie.load("carts").slice(-1)[0].subtotal})
  // this.getOrderItems();
  this.getOrderNo();
}

getOrderNo=()=>{
  let mill = String(new Date().getMilliseconds());
  let min = String(new Date().getMinutes());
  let hour = String(new Date().getHours());
  let date = String(new Date().getDate());
  let month = String(new Date().getMonth() + 1);
  let year = String(new Date().getFullYear());
  let orderN=Number(year+month+date+hour+min+mill);
  this.setState({orderNo:(orderN)});
}
paymentChoose=(pay)=>{
  switch(pay){
    case 'bank':
      this.setState({payment:'bank'});
      break;
    case 'paypal':
      this.setState({payment:'paypal'});
      break;
  }

}
// // getOrderItems=()=>{
// //   cookie.load('carts').map((o,i)=>{
// //     this.setState({orderItems:[...this.state.orderItems,o.sku]})
// //   })
// }
submitOrder=(order)=>{
  if (order){
    if(order.status==='COMPLETED'){
this.setState({orderStatus:'Paid'});
this.setState({payment:'paypal'});

    }
  }
  Axios.post('http://chvast.com:3001/checkout',{
    orderNo:this.state.orderNo,
    firstName:this.firstName.current.value,
    lastName :this.lastName.current.value,
    email :this.email.current.value,
    address1 :this.address1.current.value,
    address2 :this.address2.current.value,
    city :this.city.current.value,
    zip :this.zip.current.value,
    payment :this.state.payment,
    shippingFee :this.shippingFee,
    subtotal :this.state.subtotal,
    orderStatus :this.state.orderStatus
  }).then((Response)=>{
    Axios.post('http://chvast.com:3001/order-items',{
    orderNo:this.state.orderNo,
    items:cookie.load("carts")
  })
    if(Response.data==='success')
    {
      
      this.props.history.push("/thank-you");
    }
  })
}


  render() {
    return (
      <>
        <Container fluid>
          
          <Row>
            <Col xs="10" className="checkout-title">
              <h3>secure checkout</h3>
            </Col>
          </Row>
         
          
            
              <Form>
                  
              <Row>
              <Col xs="12" md="6" className="checkout-bill-info">
                <Form.Row>
                  <Form.Group as={Col} controlId="formGridFirstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control required placeholder="First Name" ref={this.firstName} />
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridLastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control required placeholder="Last Name" ref={this.lastName} />
                  </Form.Group>
                </Form.Row>

                <Form.Group controlId="formGridEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    required
                    type="Email"
                    placeholder="Enter Email"
                    ref={this.email}
                  />
                </Form.Group>
                <Form.Group controlId="formGridAddress1">
                  <Form.Label>Address</Form.Label>
                  <Form.Control
                    required
                    placeholder="House Number and Street/Road"
                    ref={this.address1}
                  />
                </Form.Group>

                <Form.Group controlId="formGridAddress2">
                  <Form.Label>Address Line 2</Form.Label>
                  <Form.Control placeholder="Optional: Apartment, studio, or floor" ref={this.address2} />
                </Form.Group>

                <Form.Row>
                  <Form.Group as={Col} controlId="formGridCity">
                    <Form.Label>City</Form.Label>
                    <Form.Control required ref={this.city} />
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridZip">
                    <Form.Label>Post Code/Zip</Form.Label>
                    <Form.Control required ref={this.zip} />
                  </Form.Group>
                </Form.Row>

                
            </Col>


            <Col xs="12" md="6" className="checkout-payment-info">
              <div className="payment-box">
                <div className="payment-head">
                  <h4>Order Summary</h4>
                </div>

                <div className="payment-title">
                  <ul className="list-unstyled">
                    <li>Product</li>
                    <li>Total</li>
                  </ul>
                  <div className="divider"></div>
                </div>
                {
                  cookie.load('carts').map((pro,index)=>{
                    return(
                      <ul className="list-unstyled payment-list">
                      <li>
                        <span>
                        <span><strong>{pro.count} x </strong>  </span> {pro.name }{pro.color} {pro.storages} {pro.grade}
                        </span>
                        <span className="individual-price">${pro.total}.00</span>
                      </li>
                      
                    </ul>
                    )
                   
                  })
                }
              
            
                <div className="divider"></div>
                <ul className="list-unstyled payment-list">
                  <li>
                    <b>Delivery:</b>
                    <span>Free Shipping</span>
                  </li>
                </ul>
                <div className="divider"></div>
                <ul className="list-unstyled payment-list">
                  <li>
                    <b>Subtotal:</b>
                    <span className="total-price">${this.state.subtotal}.00
                             </span>
                  </li>
                </ul>
                <div className="divider"></div>
              <Form.Group>
                <Form.Check type='radio'  id='bank'>
                  <Form.Check.Input type='radio' name='payment' defaultChecked onChange={this.paymentChoose.bind(this,'bank')} />
                  <Form.Check.Label><b>Direct bank transfer</b> </Form.Check.Label>
                </Form.Check>
                <div>
                    Please make your payment directly into our bank account and
                    email us your contact number once you make the payment.
                    Please use your Order ID as the payment reference. We will
                    process your order when we receive your fund.
                  </div>
                  <Form.Check type='radio'  id='paypal' className='paypal' >
                  <Form.Check.Input type='radio' name='payment' onChange={this.paymentChoose.bind(this,'paypal')} />
                  <Form.Check.Label> <b>Paypal</b> </Form.Check.Label>
                  <img src={paypal} alt="" />
                </Form.Check>
                </Form.Group>
              </div>
              {/* ------------------------------------payment box end--------------------------- */}
              <Form.Group id="formGridCheckbox">
                  <Form.Check type="checkbox" label=" I have read and agree to the website terms and conditions *" />
                </Form.Group>
              {
                this.state.payment==='paypal' ? <Paypal subtotal={this.state.subtotal} submitOrder={this.submitOrder} /> : <Button variant="primary"  className="place-order-btn" onClick={this.submitOrder}>
                PLACE ORDER
              </Button>
              }
         
            </Col>
            
                </Row>
               
              </Form>
          </Container>
        
      </>
    );
  }
}

export default Checkout;
