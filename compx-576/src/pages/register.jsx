import React, { Component } from "react";
import {
  Form,
  Button,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import Axios from "axios";
import MD5 from 'crypto-js/md5';
class Register extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",

  };


  setFirstName = (e) => {
    e.preventDefault();
    this.setState({ firstName: e.target.value }, () => {
      console.log(this.state.firstName);
    });
  };
  setLastName = (e) => {
    e.preventDefault();
    this.setState({ lastName: e.target.value }, () => {
      console.log(this.state.lastName);
    });
  };
  setEmail = (e) => {
    e.preventDefault();
    this.setState({ email: e.target.value }, () => {
      console.log(this.state.email);
    });
  };
  setPassword = (e) => {
    e.preventDefault();
    this.setState({ password: e.target.value }, () => {
      console.log(this.state.password);
    });
  };
  registerForm = () => {
    Axios.post("http://chvast.com:3001/register", {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: MD5(this.state.password).toString(),
    }).then(() => {
      alert('registration success.');
    });
  };
  render() {
    return (
      <>
        <Container>
          <Row className="justify-content-md-center">
            <Col xs md="8">
              <div className="register-title">
                <h3>Join Mobile Outlet</h3>
              </div>
              <Form>
                <Form.Group controlId="formGridFirstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    required
                    placeholder="First Name"
                     onChange={this.setFirstName}
                    
                  />
                </Form.Group>

                <Form.Group controlId="formGridLastName">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                    required
                    placeholder="Last Name"
                    onChange={this.setLastName}
                  />
                </Form.Group>

                <Form.Group controlId="formGridEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    required
                    type="Email"
                    placeholder="Enter Email"
                    onChange={this.setEmail}
                  />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    onChange={this.setPassword}
                  />
                </Form.Group>
                <Button
                  type="submit"
                  className="btn btn-warning btn-primary-hover register-button"
                  onClick={this.registerForm}
                >
                  Create Account
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Register;
