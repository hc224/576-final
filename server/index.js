const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");
app.use(cors());
app.use(express.json());
const db = mysql.createConnection({
  user: "phpmyadmin",
  host: "192.168.186.133",
  password: "642123",
  database: "MobileOutlet",
});

app.post("/input-brand", (req, res) => {
  console.log("input-brand");
  const brand = req.body.brand;
  db.query(
    "insert into product_brand (brand_name) values (?)",
    [brand],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("brand inserted");
      }
    }
  );
});

app.post("/input-categories", (req, res) => {
  console.log("input-ca");
  const category = req.body.categories;
  db.query(
    "insert into product_category (category_name) values (?)",
    [category],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("category inserted");
      }
    }
  );
});
app.post("/input-spec", (req, res) => {
  const spec = req.body.spec;
  db.query(
    "insert into product_spec (spec_name) values (?)",
    [spec],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("spec inserted");
      }
    }
  );
});
app.post("/input-spec-value", (req, res) => {
  const specValue = req.body.specValue;
  const specID = req.body.specID;
  db.query(
    "insert into product_spec_value (spec_value,spec_id) values (?,?)",
    [specValue, specID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("spec value inserted");
      }
    }
  );
});
app.post("/input-cate-spec", (req, res) => {
  const categoryID = req.body.categoryID;
  const specID = req.body.specID;
  db.query(
    "insert into category_spec (category_id,spec_id) values (?,?)",
    [categoryID, specID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("cate spec value inserted");
      }
    }
  );
});
app.post("/input-spu", (req, res) => {
  console.log("input-spu");
  const newSPU = req.body.newSPU;
  const spuNo = req.body.spuNo;
  const categoryID = req.body.categoryID;
  const brandID = req.body.brandID;
  db.query(
    "insert into product_spu (spu_no,product_name,category_id,brand_id) values (?,?,?,?)",
    [spuNo, newSPU, categoryID, brandID],
    (err, result) => {
      if (err) {
        console.log(err);
        res.send(err);
      } else {
        res.send("success input spu");
      }
    }
  );
});
app.post("/input-desc", (req, res) => {
  console.log("input-spu");
  let desc = String(req.body.description);
  let sku = req.body.sku;

  db.query(
    "insert into sku_description(sku_id,description) values (?,?)",
    [sku, desc],
    (err, result) => {
      if (err) {
        console.log(err);
        res.send(err);
      } else {
        res.send("success");
      }
    }
  );
});
app.post("/input-sku", (req, res) => {
  console.log("input-spu");
  const spu = req.body.spu;
  const color = req.body.color;
  const grade = req.body.grade;
  const storages = req.body.storages;
  const warranty = req.body.warranty;
  const SKUNo = req.body.SKUNo;
  const regularPrice = req.body.regularPrice;
  const salePrice = req.body.salePrice;
  const stock = req.body.stock;
  const pic = req.body.pic;
  db.query(
    "insert into product_sku (spu_id,color,grade,storages,warranty,sku_no,regular_price,sale_price,stock,pic_src) values (?,?,?,?,?,?,?,?,?,?)",
    [
      spu,
      color,
      grade,
      storages,
      warranty,
      SKUNo,
      regularPrice,
      salePrice,
      stock,
      pic,
    ],
    (err, result) => {
      if (err) {
        console.log(err);
        res.send(err);
      } else {
        res.send("success");
      }
    }
  );
});
app.post("/delete-categories", (req, res) => {
  console.log("dele");
  const delCate = req.body.delCate;
  db.query(
    "delete from product_category where category_name=?",
    [delCate],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("category deleted");
      }
    }
  );
});
app.post("/delete-brand", (req, res) => {
  console.log("dele");
  const delBrand = req.body.delBrand;
  db.query(
    "delete from product_brand where brand_name=?",
    [delBrand],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("brand deleted");
      }
    }
  );
});
app.post("/delete-spu", (req, res) => {
  console.log("dele");
  const delSPU = req.body.delSPU;
  db.query("delete from product_spu where id=?", [delSPU], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send("brand deleted");
    }
  });
});
app.post("/delete-spec", (req, res) => {
  console.log("dele");
  const delSpec = req.body.delSpec;
  db.query(
    "delete from product_spec where spec_name=?",
    [delSpec],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("spec deleted");
      }
    }
  );
});
app.post("/delete-spec-value", (req, res) => {
  console.log("dele-spec-value");
  const delSpecValue = req.body.delSpecValue;
  db.query(
    "delete from product_spec_value where spec_value=?",
    [delSpecValue],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("spec deleted");
      }
    }
  );
});
app.post("/delete-cate-spec", (req, res) => {
  console.log("dele-cate-spec");
  const delCateSpec = req.body.delCateSpec;
  db.query(
    "delete from category_spec where id=?",
    [delCateSpec],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("cate spec deleted");
      }
    }
  );
});
app.post("/delete-sku", (req, res) => {
  console.log("dele-sku");
  const delsku = req.body.delsku;
  db.query(
    "delete from product_sku where sku_no=?",
    [delsku],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("sku deleted");
      }
    }
  );
});
app.post("/delete-order", (req, res) => {
  console.log("dele-order");

  const orderNo = req.body.orderNo;
  console.log(orderNo);
  db.query(
    "delete t1,t2 from order_info t1,order_items t2 where t1.order_no=t2.order_num and t1.order_no=?",
    [orderNo],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("order deleted");
      }
    }
  );
});
app.post("/alter-categories", (req, res) => {
  console.log("al-cate");
  const alCateId = req.body.alCateId;
  const categories = req.body.categories;
  db.query(
    "update product_category set category_name=? where id=?",
    [categories, alCateId],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("category alteried");
      }
    }
  );
});
app.post("/alter-brand", (req, res) => {
  const alBrandId = req.body.alBrandId;
  const brand = req.body.brand;
  db.query(
    "update product_brand set brand_name=? where id=?",
    [brand, alBrandId],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("brand alteried");
      }
    }
  );
});
app.post("/alter-spec", (req, res) => {
  const alSpecId = req.body.alSpecId;
  const spec = req.body.spec;
  db.query(
    "update product_spec set spec_name=? where id=?",
    [spec, alSpecId],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("spec alteried");
      }
    }
  );
});
app.post("/alter-spec-value", (req, res) => {
  const alSpecId = req.body.alSpecValueId;
  const specValue = req.body.specValue;
  db.query(
    "update product_spec_value set spec_value=? where id=?",
    [specValue, alSpecId],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("spec alteried");
      }
    }
  );
});
app.post("/alter-spu", (req, res) => {
  const spuID = req.body.spuID;
  const spuNo = req.body.spuNo;
  const spuCategoryID = req.body.spuCategoryID;
  const spuBrandID = req.body.spuBrandID;
  const spuProduct = req.body.spuProduct;
  db.query(
    "update product_spu set spu_no=?,category_id=?,brand_id=?,product_name=? where id=?",
    [spuNo, spuCategoryID, spuBrandID, spuProduct, spuID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("spu alteried");
      }
    }
  );
});

app.get("/get-brand", (req, res) => {
  console.log("getbrand");
  db.query("select id,brand_name from product_brand", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      // console.log(result);
      res.send(result);
    }
  });
});
app.get("/get-categories", (req, res) => {
  console.log("get cate");
  db.query("select id,category_name from product_category", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
app.get("/get-spec", (req, res) => {
  console.log("get spec");
  db.query("select id,spec_name from product_spec", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});
app.post("/get-spec-value", (req, res) => {
  const specID = req.body.specID;
  console.log("get spec value");
  db.query(
    "select id,spec_id,spec_value from product_spec_value where spec_id=?",
    [specID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.get("/get-spec-warranty", (req, res) => {
  const specID = 6;
  console.log("get spec warranty");
  db.query(
    "select id,spec_value from product_spec_value where spec_id=?",
    [specID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.get("/get-spec-color", (req, res) => {
  const specID = 1;
  console.log("get spec color");
  db.query(
    "select id,spec_value from product_spec_value where spec_id=?",
    [specID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.get("/get-spec-grade", (req, res) => {
  const specID = 3;
  console.log("get spec grade");
  db.query(
    "select id,spec_value from product_spec_value where spec_id=?",
    [specID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.get("/get-spec-storages", (req, res) => {
  const specID = 7;
  console.log("get spec storages");
  db.query(
    "select id,spec_value from product_spec_value where spec_id=?",
    [specID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.post("/get-spec-select", (req, res) => {
  const spuID = req.body.spuID;
  console.log("get spec value");
  db.query(
    "Select t1.id,t1.spec_name from product_spec t1 left join category_spec t2 on t1. id=t2.spec_id left join product_spu t3 on t2.category_id=t3.category_id where t3.id=?",
    [spuID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.post("/get-brand-category", (req, res) => {
  const spuID = req.body.spuID;
  console.log("get spec value");
  db.query(
    "Select t2.category_name,t3.brand_name from product_spu t1 left join product_category t2 on t1.category_id=t2.id left join product_brand t3 on t1.brand_id=t3.id where t1.id=?",
    [spuID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.get("/get-user", (req, res) => {
  console.log("get user");
  db.query(
    "Select id,first_name,last_name,email,date_register from user_info",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.post("/get-user-shipping", (req, res) => {
  console.log("get user shipping");
  const userID = req.body.userID;
  db.query(
    "Select first_name,last_name,address1,address2,city,postcode from user_shipping_address where id=?",
    [userID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.post("/get-product-desc", (req, res) => {
  console.log("get product desc");
  const skuID = req.body.skuID;
  db.query(
    "Select description from sku_description t1 left join product_sku t2 on t1.sku_id=t2.id where t2.id=?",
    [skuID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.get("/get-user-order", (req, res) => {
  console.log("get order");
  db.query(
    "Select order_no,first_name,last_name,order_time,order_status,subtotal from order_info",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.post("/get-user-order-detail", (req, res) => {
  console.log("get order detail");
  const orderNo = req.body.orderNo;
  console.log(orderNo);
  db.query(
    "Select first_name,last_name,order_time,order_status,subtotal,address1,address2,city,zip,order_time,payment from order_info where order_no=?",
    [orderNo],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.post("/get-sku", (req, res) => {
  const skuID = req.body.sku;
  if (skuID) {
    db.query(
      "Select t1.id,t1.sku_no,t1.regular_price,t1.sale_price,t1.stock,t1.color,t1.grade,t1.storages,t2.product_name from product_sku t1 left join product_spu t2 on t1.spu_id=t2.id where t1.id=?",
      [skuID],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  } else {
    db.query(
      "Select t1.id,t1.sku_no,t1.regular_price,t1.sale_price,t1.stock,t1.color,t1.grade,t1.storages,t2.product_name from product_sku t1 left join product_spu t2 on t1.spu_id=t2.id",
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  }
  console.log("get sku");
});
app.post("/get-cate-spec-value", (req, res) => {
  const cateChange = req.body.categoryChange;
  if (cateChange) {
    db.query(
      "Select t1.id,t2.category_name,t3.spec_name from category_spec t1 left join product_category t2 on t1.category_id=t2.id left join product_spec t3 on t1.spec_id=t3.id where t2.id=?",
      [cateChange],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  } else {
    console.log("get cate spec value");
    db.query(
      "Select t1.id,t2.category_name,t3.spec_name from category_spec t1 left join product_category t2 on t1.category_id=t2.id left join product_spec t3 on t1.spec_id=t3.id;",
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  }
});
app.post("/get-spu", (req, res) => {
  const categoryID = req.body.categoryID;
  const brandID = req.body.brandID;
  console.log(categoryID);
  console.log(brandID);
  if (categoryID && brandID) {
    db.query(
      "select t1.id,t1.spu_no,t1.product_name,t2.category_name,t3.brand_name from product_spu t1 left join product_category t2 on t1.category_id=t2.id left join product_brand t3 on t1.brand_id=t3.id where t1.category_id=? and t1.brand_id=?",
      [categoryID, brandID],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  } else if (categoryID && brandID == "") {
    db.query(
      "select t1.id,t1.spu_no,t1.product_name,t2.category_name,t3.brand_name from product_spu t1 left join product_category t2 on t1.category_id=t2.id left join product_brand t3 on t1.brand_id=t3.id where t1.category_id=?",
      [categoryID],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  } else if (categoryID == "" && brandID) {
    db.query(
      "select t1.id,t1.spu_no,t1.product_name,t2.category_name,t3.brand_name from product_spu t1 left join product_category t2 on t1.category_id=t2.id left join product_brand t3 on t1.brand_id=t3.id where t1.brand_id=?",
      [brandID],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  } else {
    db.query(
      "select t1.id,t1.spu_no,t1.product_name,t2.category_name,t3.brand_name from product_spu t1 left join product_category t2 on t1.category_id=t2.id left join product_brand t3 on t1.brand_id=t3.id",
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          console.log(result);
          res.send(result);
        }
      }
    );
  }
});
// ----------------------------------userMangament---------------------------------

app.post("/register", (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  console.log(password);
  let mill = new Date().getMilliseconds();
  let min = new Date().getMinutes();
  let hour = new Date().getHours();
  let date = new Date().getDate();
  let month = new Date().getMonth() + 1;
  let year = new Date().getFullYear();
  let registerTime =
    hour + ":" + min + ":" + mill + " " + date + "/" + month + "/" + year;
  db.query(
    "insert user_info (first_name,last_name,email,password,address_id,date_register) values (?,?,?,?,?,?)",
    [firstName, lastName, email, password, -1, registerTime],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});
app.post("/delete-user", (req, res) => {
  console.log("dele-user");
  const deluser = req.body.deluser;
  db.query(
    "delete from user_info where id=?",
    [deluser],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("sku deleted");
      }
    }
  );
});
app.post("/get-user-info", (req, res) => {
  const userID = req.body.userID;
  db.query(
    "select first_name,last_name,email,password,address_id from user_info where id=?",
    [userID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// ------------------------------------front end----------------------------------

app.get("/get-home-products", (req, res) => {
  db.query(
    "select t1.id,t1.regular_price,t1.sale_price,t1.storages,t1.color,t1.grade,t1.pic_src,t3.brand_name,t4.product_name from product_sku t1 left join product_spu t2 on t1.spu_id=t2.id left join product_brand t3 on t2.brand_id=t3.id left join product_spu t4 on t1.spu_id=t4.id",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});
app.post("/get-sku-desc", (req, res) => {
  const skuID = req.body.skuID;
  db.query(
    "select t1.id,t1.regular_price,t1.sale_price,t1.storages,t1.color,t1.grade,t1.warranty,t1.stock,t1.pic_src,t3.brand_name,t4.product_name from product_sku t1 left join product_spu t2 on t1.spu_id=t2.id left join product_brand t3 on t2.brand_id=t3.id left join product_spu t4 on t1.spu_id=t4.id left join sku_description t5 on t1.id=t5.sku_id where t1.id=?",
    [skuID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});
app.post("/user-login", (req, res) => {
  const email = req.body.email;
  const pwd = req.body.pwd;
  console.log(email);
  console.log(pwd);
  db.query(
    "select id,first_name from user_info where email=? and password=?",
    [email, pwd],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});
app.post("/checkout", (req, res) => {
  const orderNo = req.body.orderNo;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const address1 = req.body.address1;
  const address2 = req.body.address2;
  const city = req.body.city;
  const zip = req.body.zip;
  const payment = req.body.payment;
  const shippingFee = req.body.shippingFee;
  const subtotal = req.body.subtotal;
  const orderStatus = req.body.orderStatus;

  db.query(
    "insert into order_info(order_no,first_name,last_name,email,address1,address2,city,zip,payment,shipping_fee,subtotal,order_status) values(?,?,?,?,?,?,?,?,?,?,?,?)",
    [
      orderNo,
      firstName,
      lastName,
      email,
      address1,
      address2,
      city,
      zip,
      payment,
      shippingFee,
      subtotal,
      orderStatus,
    ],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("success");
        console.log(result);
      }
    }
  );
});
app.post("/order-items", (req, res) => {
  const items = req.body.items;
  console.log('-------');
  console.log(items);
  console.log('-------------');
  const orderNo = req.body.orderNo;
  items.map((item,i)=>{
    db.query(
      "insert into order_items(order_num,item_sku,quantity,total) values(?,?,?,?)",
      [orderNo,item.sku,item.count,item.total],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
         // res.send(result);
          console.log(result);
        }
      }
    );
  })
 
});
app.listen(3001, () => {
  console.log("yes! its start!");
});
