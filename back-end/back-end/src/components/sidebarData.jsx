import React from 'react';
import {Cart4,Award,Tags,ClipboardMinus,Diagram3,JournalMinus,LifePreserver,PersonLinesFill} from "react-bootstrap-icons";
export const SidebarData=[
    {
        title:'Order',
        path:'/user-order',
        icon:<Cart4 />
    }
]

export const Products=   [ 
    {
        icon: <Award/>,
        title:'Product Brand',
        path:'/product-brand'
    },
    {
        icon: <Tags/>,
        title:'Product Categories',
        path:'/product-categories'
    },
    {
        icon: <ClipboardMinus/>,
        title:'Product Specification',
        path:'/product-spec'
    },
    {
        icon: <Diagram3/>,
        title:'Product SPU',
        path:'/product-spu'
    },
    {
        icon: <JournalMinus/>,
        title:'Category Specification',
        path:'/category-spec'
    },
    {
        icon: <LifePreserver/>,
        title:'Product SKU',
        path:'/product-sku'
    }
]
export const Customers=   [
    {
        icon: <PersonLinesFill/>,
        title:'Users',
        path:'/user-management'
    }
]