import React, { useState } from 'react';
import { Link } from 'react-router-dom';


function SidebarDropdown(props) {
   
   
    const [ItemSelected, setItemSelected]=useState(0);
  
    const itemSW=(sw)=>{return(()=>{setItemSelected(sw)})}
    return(
        <>
        <ul>
        {
            
            props.item.map((item,index)=>{
                return(
                    <li key={index} className={ItemSelected===index ? 'nav-text selected dropdown':'nav-text dropdown'} onClick={itemSW(index)}>
                        <Link to={item.path} >
                       {item.icon} {item.title}
                        </Link>

                    </li>
                )
            })
        }
        </ul>
        </>
    )
}
export default SidebarDropdown;