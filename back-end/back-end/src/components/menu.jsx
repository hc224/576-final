import React, {  useState } from "react";
import { Link } from "react-router-dom";
import { BagPlus,People, MenuButtonWide, XCircle } from "react-bootstrap-icons";
import {
  Container,
  Row,
  Col,
} from "react-bootstrap";
import { SidebarData, Products, Customers } from "./sidebarData";
import "../css/menu.css";
import SidebarDropdown from "./sidebardropdown";
import logo from "../img/mobile_outlet4-small.png";
import userLogo from "../img/mobile_outlet_circle.png";

function Menu(props) {
  const [sidebar, setSidebar] = useState(false); //open or close sidebar
  const showSidebar = () => setSidebar(!sidebar);
  // const [sidebarDropdown, setSidebarDropdown] = useState(false); //open or close sidebar dropdown
  // const showDropdown=()=> setSidebarDropdown(!sidebarDropdown)

  const [sidebarItem, setSidebarItem] = useState(0); //which sidebar item will be seleted
  const sidebarItemSW = (sw) => {
    return () => {
      setSidebarItem(sw);
    };
  };

  return (
    <>
      <Container fluid>
        <Row className="back-header">
          <Col>
            <Link to="/" className="logo-link">
              <img src={logo} className="logo" alt='logo' />
            </Link>
          </Col>
          <Col></Col>
          <Col>
            <Link to="#" className="menu-toggle">
              <MenuButtonWide onClick={() => showSidebar()} />
            </Link>
          </Col>
        </Row>
      </Container>

      <nav className={sidebar ? "nav-menu active" : "nav-menu"}>
        <ul className="nav-menu-items">
          <li className="navbar-toggle">
            <Link to="#" className="menu-close">
              <XCircle onClick={() => showSidebar()} />
            </Link>
          </li>
          <li className="menu-user">
            <img src={userLogo} alt="" />
            <span>
              <div className='menu-userName'>Luke Cheng</div>
              <div className='menu-userRole'>ADMINISTRATOR</div>
            </span>
          </li>
          <li className={sidebarItem === 1 ? "nav-text selected" : "nav-text"}>
            <Link to="#" onClick={() => setSidebarItem(1)}>
              <BagPlus />
              Products
            </Link>
          </li>
          {sidebarItem === 1 && <SidebarDropdown item={Products} />}

          <li className={sidebarItem === 2 ? "nav-text selected" : "nav-text"}>
            <Link to="#" onClick={() => setSidebarItem(2)}>
              <People />
              Customers
            </Link>
          </li>
          {sidebarItem === 2 && <SidebarDropdown item={Customers} />}

          {SidebarData.map((item, index) => {
            return (
              <li
                key={index}
                className={
                  sidebarItem === index + 3 ? "nav-text selected" : "nav-text"
                }
              >
                <Link to={item.path} onClick={sidebarItemSW(index + 3)}>
                  {item.icon}
                  {item.title}
                </Link>
              </li>
            );
          })}
        </ul>
      </nav>
    </>
  );
}

export default Menu;
