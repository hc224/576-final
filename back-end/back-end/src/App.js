import React from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Menu from "./components/menu";
import "./css/App.css";

import NotFound from "./pages/404";
import ProductBrand from "./pages/product_Brand";
import "./css/content-page.css";
import ProductCategories from "./pages/productCategories";
import ProductSPU from "./pages/product_SPU";
import UserManagement from "./pages/user_management";
import ProductSpec from "./pages/product_Spec";
import CategorySpec from "./pages/category_specification";
import ProductSKU from "./pages/product_sku";
import ProductDetail from "./pages/product_detail";
import UserOrder from "./pages/user_order";

function App() {
  return (
    <>
      <div className="Back">
        <Router>
          <Menu />

          <Switch>
            <Route exact path="/product-brand" component={ProductBrand} />
            <Route
              exact
              path="/product-categories"
              component={ProductCategories}
            />
            <Route exact path="/product-spu" component={ProductSPU} />
            <Route exact path="/user-management" component={UserManagement} />
            <Route exact path="/product-spec" component={ProductSpec} />
            <Route exact path="/category-spec" component={CategorySpec} />
            <Route exact path="/product-sku" component={ProductSKU} />
            <Route exact path="/product-detail" component={ProductDetail} />
            <Route exact path="/user-order" component={UserOrder} />

            <Route component={NotFound} />
          </Switch>
        </Router>
      </div>
    </>
  );
}

export default App;
