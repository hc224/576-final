import React, { useEffect, useState, useRef } from "react";
import { Link } from "react-router-dom";
import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
  Modal,
} from "react-bootstrap";
import Axios from "axios";
import "../css/productSKU.css";
import Upload_s3 from "../components/upload_s3";

function Product_sku() {
  const [spuList, setspuList] = useState([]);
  const [skuList, setskuList] = useState([]);
  const [deleteSKU, setdeleteSKU] = useState(-1);

  const [specColor, setspecColor] = useState([]);
  const [specGrade, setspecGrade] = useState([]);
  const [specStorages, setspecStorages] = useState([]);
  const [specWarranty, setspecWarranty] = useState([]);
  //   const [specSelect, setspecSelect] = useState(-1);
  const [category, setcategory] = useState("");
  const [brand, setbrand] = useState("");
  const [showSpec, setshowSpec] = useState(false);

  const [uploadedImg, setuploadedImg] = useState(null);
  const [uploadedImgFile, setuploadedImgFile] = useState(null);

  const [SPU, setSPU] = useState(null);
  const color = useRef(null);
  const grade = useRef(null);
  const storages = useRef(null);
  const warranty = useRef(null);
  const SKUNo = useRef(null);
  const regularPrice = useRef(null);
  const salePrice = useRef(null);
  const stock = useRef(null);
  // const [pic, setpic] = useState("");

  const [deleteShow, setdeleteShow] = useState(false);
  const deleteCloseHandle = () => setdeleteShow(false);
  const deleteShowHandle = () => setdeleteShow(true);

  const getSPU = () => {
    Axios.post("http://chvast.com:3001/get-spu").then((Response) => {
      setspuList(Response.data);
    });
  };
  const getSKU = () => {
    Axios.post("http://chvast.com:3001/get-sku").then((Response) => {
      setskuList(Response.data);
      console.log(skuList);
    });
  };
  //   const getSpec = () => {};

  const specChange = (spuID) => {
    setSPU(spuID);
    setshowSpec(true);
    Axios.post("http://chvast.com:3001/get-brand-category", {
      spuID: spuID,
    }).then((Response) => {
      setbrand(Response.data[0].brand_name);
      setcategory(Response.data[0].category_name);
    });
  };
  //   const getSpecValue = (specID) =>{
  //     Axios.post("http://chvast.com:3001/get-spec-value", {
  //       specID: specID,
  //     }).then((Response) => {
  //       setspecValueList(Response.data);
  //     });

  //     console.log(specValueList);

  //   }

  const getSpecValues = () => {
    Axios.get("http://chvast.com:3001/get-spec-warranty").then((Response) => {
      setspecWarranty(Response.data);
      console.log(specWarranty);
    });
    Axios.get("http://chvast.com:3001/get-spec-storages").then((Response) => {
      setspecStorages(Response.data);
      console.log(specStorages);
    });
    Axios.get("http://chvast.com:3001/get-spec-grade").then((Response) => {
      setspecGrade(Response.data);
      console.log(specGrade);
    });
    Axios.get("http://chvast.com:3001/get-spec-color").then((Response) => {
      setspecColor(Response.data);
      console.log(specColor);
    });
  };


  const addSKU = () => {
    if (uploadedImgFile) {
      Upload_s3(uploadedImgFile)
        .then((location) => {
          Axios.post("http://chvast.com:3001/input-sku", {
            spu: SPU,
            color: color.current.value,
            grade: grade.current.value,
            storages: storages.current.value,
            warranty: warranty.current.value,
            SKUNo: SKUNo.current.value,
            regularPrice: regularPrice.current.value,
            salePrice: salePrice.current.value,
            stock: stock.current.value,
            pic: location,
          }).then((Response) => {
            if ((Response = "success")) {
              alert("success to add SKU.");
              getSKU();
            } else {
              alert("something wrong.");
            }
          })
        })
       
    } else {
      alert("please upload product image.");
    }
  };
  const deleteConfirm = (skuNo) => {
    setdeleteSKU(skuNo);
    console.log(skuNo);
    deleteShowHandle();
  };
  const deleteSKUFun = () => {
    Axios.post("http://chvast.com:3001/delete-sku", {
      delsku: deleteSKU,
    }).then(() => {
      getSKU();
      alert("delete successfully.");
      deleteCloseHandle();
    });
  };
  const showImage = (file) => {
    setuploadedImgFile(file);
    setuploadedImg(URL.createObjectURL(file));
  };

  const SpecItems = () => {
    return (
      <>
        <div className="content-item-title">Specification:</div>
        <div class="spec-box">
          <div className="content-item-title">Color:</div>

          <Row>
            <Col xs={12} lg={6}>
              <Form.Control as="select" ref={color}>
                {specColor.map((color, index) => {
                  return (
                    <option value={color.spec_value}>{color.spec_value}</option>
                  );
                })}
              </Form.Control>
            </Col>
          </Row>

          <div className="content-item-title">Storages:</div>
          <Row>
            <Col xs={12} lg={6}>
              <Form.Control as="select" ref={storages}>
                {specStorages.map((storages, index) => {
                  return (
                    <option value={storages.spec_value}>
                      {storages.spec_value}
                    </option>
                  );
                })}
              </Form.Control>
            </Col>
          </Row>

          <div className="content-item-title">Grade:</div>

          <Row>
            <Col xs={12} lg={6}>
              <Form.Control as="select" ref={grade}>
                {specGrade.map((grade, index) => {
                  return (
                    <option value={grade.spec_value}>{grade.spec_value}</option>
                  );
                })}
              </Form.Control>
            </Col>
          </Row>

          <div className="content-item-title">Warranty:</div>
          <Row>
            <Col xs={12} lg={6}>
              <Form.Control as="select" ref={warranty}>
                {specWarranty.map((warranty, index) => {
                  return (
                    <option value={warranty.spec_value}>
                      {warranty.spec_value}
                    </option>
                  );
                })}
              </Form.Control>
            </Col>
          </Row>
        </div>
      </>
    );
  };

  useEffect(() => {
    getSPU();
    getSpecValues();
    getSKU();
  }, []);

  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">Product SKU</div>
        <div className="content-cardbox">
          <Row>
            <Col xs={6}>
              <div className="content-item-title">SPU:</div>
              <Form.Control
                size="sm"
                as="select"
                onChange={(e) => specChange(e.target.value)}
              >
                <option value="">---</option>
                {spuList.map((spu, index) => {
                  return (
                    <>
                      <option value={spu.id}>{spu.product_name}</option>
                    </>
                  );
                })}
              </Form.Control>
            </Col>
            <Col xs={6}>
              <div className="content-item-title">SKU No.:</div>
              <Form.Control size="sm" ref={SKUNo}></Form.Control>
            </Col>
          </Row>
          {showSpec && <SpecItems />}

          <Row>
            <Col xs={6}>
              <div className="content-item-title">Regular Price:</div>
              <Form.Control size="sm" ref={regularPrice}></Form.Control>
            </Col>
            <Col xs={6}>
              <div className="content-item-title">Stock:</div>
              <Form.Control size="sm" ref={stock}></Form.Control>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <div className="content-item-title">Sale Price:</div>
              <Form.Control size="sm" ref={salePrice}></Form.Control>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <div className="content-item-title">Picture:</div>
              <Form.Control
                size="sm"
                type="file"
                onChange={(e) => showImage(e.target.files[0])}
              ></Form.Control>
            </Col>
          </Row>
          <Row>
            <Col>
              <img src={uploadedImg} alt="product-img" className='product-img'/>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button className="add-sku-btn" onClick={() => addSKU()}>
                Add
              </Button>
            </Col>
          </Row>
        </div>
        <div className="content-cardbox">
          <Table>
            <thead>
              <tr>
                <th>SKU No.</th>
                <th>SPU</th>
                <th>Color</th>
                <th>Storage</th>
                <th>Grade</th>
                <th>Regular Price</th>
                <th>Sale Price</th>
                <th>Stock</th>
                <th>EDIT</th>
              </tr>
            </thead>
            <tbody>
              {skuList.map((sku, index) => {
                return (
                  <tr className="link-style">
                    <td>
                      <Link
                        to={{
                          pathname: "/product-detail",
                          state: { sku: sku.id },
                        }}
                      >
                        {sku.sku_no}
                      </Link>
                    </td>
                    <td>
                      <Link
                        to={{
                          pathname: "/product-detail",
                          state: { sku: sku.id },
                        }}
                      >
                        {sku.product_name}
                      </Link>
                    </td>
                    <td>{sku.color}</td>
                    <td>{sku.storages}</td>
                    <td>{sku.grade}</td>
                    <td>{sku.regular_price}</td>
                    <td>{sku.sale_price}</td>
                    <td>{sku.stock}</td>
                    <td>
                      <Button
                        variant="danger"
                        onClick={() => deleteConfirm(sku.sku_no)}
                        className="edit-btn"
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>
      </Container>

      <Modal show={deleteShow} onHide={() => deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to <u>DELETE</u> <strong>{deleteSKU}</strong>? It will
          influence related SPU! You will need to re-edit related SPU.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => deleteSKUFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default Product_sku;
