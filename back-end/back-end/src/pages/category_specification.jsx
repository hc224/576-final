import React, { useEffect, useState, useRef } from "react";

import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
  Modal,
} from "react-bootstrap";

import Axios from "axios";

function ProductSpec() {
  const [categoriesList, setcategoriesList] = useState([]);
  const [specList, setspecList] = useState([]);
  const [cateSpecList, setcateSpecList] = useState([]);
  const [deleteCateSpec, setDeleteCateSpec] = useState("");
  const [categoryChange, setcategoryChange] = useState(0);
  const categoryChosen = useRef(null);
  const specChosen = useRef(null);

  const [deleteShow, setdeleteShow] = useState(false);
  const deleteCloseHandle = () => setdeleteShow(false);
  const deleteShowHandle = () => setdeleteShow(true);

  const addCateSpecValueFun = () => {
    Axios.post("http://chvast.com:3001/input-cate-spec", {
      specID: specChosen.current.value,
      categoryID: categoryChosen.current.value,
    }).then(() => {
      getCateSpec();
      success(specChosen.current.value, "add");
    });
  };
  const deleteConfirm = (Catespec) => {
    setDeleteCateSpec(Catespec);
    deleteShowHandle();
  };
  const deleteCateSpecFun = () => {
    Axios.post("http://chvast.com:3001/delete-cate-spec", {
      delCateSpec: deleteCateSpec,
    }).then(() => {
      getCateSpec();
      success(deleteCateSpec, "del");
      setDeleteCateSpec("");
      deleteCloseHandle();
    });
  };

  const getSpec = () => {
    Axios.get("http://chvast.com:3001/get-spec").then((Response) => {
      setspecList(Response.data);
      console.log(Response.data);
    });
  };
  const getCategories = () => {
    Axios.get("http://chvast.com:3001/get-categories").then((Response) => {
      setcategoriesList(Response.data);
      console.log(Response.data);
    });
  };
  const getCateSpec = () => {
    Axios.post("http://chvast.com:3001/get-cate-spec-value", {
      categoryChange: categoryChange,
    }).then((Response) => {
      setcateSpecList(Response.data);
      console.log(Response.data);
    });
  };

  useEffect(() => {
    getSpec();
    getCategories();
    getCateSpec();
    console.log("useEffect!!!");
  }, [specList.id]);

  useEffect(() => {

    getCateSpec();
    console.log("888!");
  }, [categoryChange]);

  const success = (content, act) => {
    switch (act) {
      case "add":
        alert("Success! \n" + "Added new Category Spec: " + content);
        break;
      case "del":
        alert("Success! \n" + "Deleted Category Spec: " + content);
        break;
      case "alt":
        alert("Success! \n" + "Altered Category Spec: " + content);
        break;
    }
  };

  const EditCateSpecValue = () => {
    return (
      <>
        {cateSpecList.map((cateSpec, index) => {
          return (
            <>
              <tr>
                <td>{cateSpec.category_name}</td>
                <td>{cateSpec.spec_name}</td>
                <td>
                  <Button
                    variant="danger"
                    onClick={() => deleteConfirm(cateSpec.id)}
                    className="edit-btn col-h-xs-10"
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            </>
          );
        })}
      </>
    );
  };

  // ---------------------------------------main render-----------------------------
  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">Category Specification</div>

        <div className="content-cardbox">
          <Row>
            <Col xs={4} xl={3} className="content-item-title">
              Category Specification value:
            </Col>
            <Col xs={2} xl={2}>
              <Form.Control as="select" ref={categoryChosen}>
                {categoriesList.map((cate, index) => {
                  return <option value={cate.id}>{cate.category_name}</option>;
                })}
              </Form.Control>
            </Col>
            <Col xs={3} xl={3}>
              <Form.Control as="select" ref={specChosen}>
                {specList.map((spec, index) => {
                  return <option value={spec.id}>{spec.spec_name}</option>;
                })}
              </Form.Control>
            </Col>
            <Col xs={3} xl={4} variant="warning">
              <Button onClick={() => addCateSpecValueFun()}>Submit</Button>
            </Col>
          </Row>
        </div>

        <Row>
          {/* ---------------------------cate spec-------------------------------------- */}
          <Col xs={12} lg={5}>
            <div className="content-cardbox">
              <div className="content-item-title">Select Category:</div>
              <Form.Control
                as="select"
                onChange={(e) => setcategoryChange(e.target.value)}
              >
                <option value="">---</option>
                {categoriesList.map((category, index) => {
                  return(<>
                    <option key={index} value={category.id}>
                    {category.category_name}
                  </option>
                  </>
                  )
                 
                })}
              </Form.Control>
              <Table>
                <thead>
                  <tr>
                    <th>Category</th>
                    <th>Specification</th>
                    <th>EDIT</th>
                  </tr>
                </thead>
                <tbody>
                  <EditCateSpecValue />
                </tbody>
              </Table>
            </div>
          </Col>
          {/* ---------------------------specification value END-------------------------------------- */}
        </Row>
      </Container>

      <Modal show={deleteShow} onHide={() => deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to <u>DELETE</u> <strong>{deleteCateSpec}</strong>? It
          will influence related SPU! You will need to re-edit related SKU.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => deleteCateSpecFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default ProductSpec;
