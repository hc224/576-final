import React, { useEffect, useState, useRef } from "react";

import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
  Modal,
} from "react-bootstrap";

import Axios from "axios";

function ProductSpec() {
  const [specList, setspecList] = useState([]);
  const [specValueList, setspecValueList] = useState([]);
  const [specA, setspecA] = useState(0);
  const [specValueA, setspecValueA] = useState(0);
  const [deleteSpec, setdeleteSpec] = useState("");
  const [deleteSpecValue, setdeleteSpecValue] = useState("");
  const specChange = useRef(null);
  const spec = useRef(null);
  const specChoose = useRef(null);
  const specValue = useRef(null);
  const [specChooseShow,setspecChooseShow] = useState(0);
  const specValueChange = useRef(null);

  const [deleteShow, setdeleteShow] = useState(false);
  const deleteCloseHandle = () => setdeleteShow(false);
  const deleteShowHandle = () => setdeleteShow(true);

  const addSpecFun = () => {
    Axios.post("http://chvast.com:3001/input-spec", {
      spec: spec.current.value,
    }).then(() => {
      getspec();
      success(spec.current.value, "add");
    });
  };
  const addSpecValueFun = () => {
    Axios.post("http://chvast.com:3001/input-spec-value", {
      specValue: specValue.current.value,
      specID: specChoose.current.value,
    }).then(() => {
      getspecValue();
      success(specValue.current.value, "add");
    });
  };
  const deleteConfirm = (spec, which) => {
    console.log(spec);
    console.log(which);
    switch (which) {
      case "spec":
        setdeleteSpec(spec);
        break;
      case "specValue":
        setdeleteSpecValue(spec);
        break;
    }
    deleteShowHandle();
  };
  const deleteSpecFun = () => {
    if (deleteSpec) {
      Axios.post("http://chvast.com:3001/delete-spec", {
        delSpec: deleteSpec,
      }).then(() => {
        getspec();
        success(deleteSpec, "del");
        setdeleteSpec('');
        deleteCloseHandle();
      });
    } else if (deleteSpecValue) {
      Axios.post("http://chvast.com:3001/delete-spec-value", {
        delSpecValue: deleteSpecValue,
      }).then(() => {
        getspecValue();
        success(deleteSpecValue, "del");
        setdeleteSpecValue('');
        deleteCloseHandle();

      });
    }
    
  };
  const alterSpec = (alSpecId, which) => {
    switch (which) {
      case "spec":
        Axios.post("http://chvast.com:3001/alter-spec", {
          spec: specChange.current.value,
          alSpecId: alSpecId,
        }).then(() => {
          getspec();
          console.log("success");
        });
        success(specChange.current.value, "alt");
        setspecA(0);
        break;
      case "specValue":
        Axios.post("http://chvast.com:3001/alter-spec-value", {
          specValue: specValueChange.current.value,
          alSpecValueId: alSpecId,
        }).then(() => {
          getspecValue();
          console.log("success");
        });
        success(specValueChange.current.value, "alt");
        setspecValueA(0);
        break;
    }
  };
  const getspec = () => {
    Axios.get("http://chvast.com:3001/get-spec").then((Response) => {
      setspecList(Response.data);
      console.log(Response.data);
    });
  };
  const getspecValue = () => {
    console.log('valueeeeeeeeeeee');
    console.log(specChooseShow);
    Axios.post("http://chvast.com:3001/get-spec-value",{
       specID:specChooseShow
    }
    ).then((Response) => {
      setspecValueList(Response.data);
      console.log(Response.data);
    });
  };

  useEffect(() => {
    getspec();
    console.log("useEffect!!!");
  }, [specList.id]);

  useEffect(() => {
    getspecValue();
  }, [specChooseShow]);

  const success = (content, act) => {
    switch (act) {
      case "add":
        alert("Success! \n" + "Added new Spec: " + content);
        break;
      case "del":
        alert("Success! \n" + "Deleted Spec: " + content);
        break;
      case "alt":
        alert("Success! \n" + "Altered Spec: " + content);
        break;
    }
  };

  const EditSpec = (props) => {
    return (
      <>
        <td>{props.val.id}</td>
        {specA === props.val.id ? (
          <>
            <td>
              <Form.Control
                type="text"
                placeholder={props.val.spec_name}
                ref={specChange}
              />
              {/* must use uncontrolled input cause controlled input will re-render when input something and conditional rendering will re-render input */}
            </td>
            <td>
              <Button
                type="submit"
                onClick={() => alterSpec(props.val.id, "spec")}
                className="edit-btn col-h-xs-10"
              >
                Confirm
              </Button>
              <Button
                variant="danger"
                type="submit"
                onClick={() => {
                  setspecA(-1);
                }}
                className="edit-btn"
              >
                Cancel
              </Button>
            </td>
          </>
        ) : (
          <>
            <td>{props.val.spec_name}</td>
            <td>
              <Button
                variant="danger"
                onClick={() => deleteConfirm(props.val.spec_name, "spec")}
                className="edit-btn col-h-xs-10"
              >
                Delete
              </Button>
              <Button
                variant="info"
                onClick={() => setspecA(props.val.id)}
                className="edit-btn col-h-xs-10"
              >
                Alter
              </Button>
            </td>
          </>
        )}
      </>
    );
  };
  const EditSpecValue = () => {
    return (
      <>
        {specValueList.map((specValue, index) => {
          return (
            <>
            <tr>
              <td>{specValue.id}</td>
              {specValueA === specValue.id ? (
                <>
                  <td>
                    <Form.Control
                      type="text"
                      placeholder={specValue.spec_value}
                      ref={specValueChange}
                    />
                  </td>
                  <td>
                    <Button
                      type="submit"
                      onClick={() => alterSpec(specValue.id, "specValue")}
                      className="edit-btn col-h-xs-10"
                    >
                      Confirm
                    </Button>
                    <Button
                      variant="danger"
                      type="submit"
                      onClick={() => {
                        setspecValueA(-1);
                      }}
                      className="edit-btn"
                    >
                      Cancel
                    </Button>
                  </td>
                </>
              ) : (
                <>
                  <td>{specValue.spec_value}</td>
                  <td>
                    <Button
                      variant="danger"
                      onClick={() => deleteConfirm(specValue.spec_value, "specValue")}
                      className="edit-btn col-h-xs-10"
                    >
                      Delete
                    </Button>
                    <Button
                      variant="info"
                      onClick={() => setspecValueA(specValue.id)}
                      className="edit-btn col-h-xs-10"
                    >
                      Alter
                    </Button>
                  </td>
                </>
              )}
            </tr></>
          );
        })}
      </>
    );
  };

  // ---------------------------------------main render-----------------------------
  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">Product Specification</div>

        <div className="content-cardbox">
          <Row className="col-heigh-16">
            <Col xs={4} xl={3} className="content-item-title">
              Product Specification:
            </Col>
            <Col xs={5} xl={5}>
              <Form.Control placeholder="like: Storage, Color" ref={spec} />
            </Col>
            <Col xs={3} xl={4} variant="warning">
              <Button onClick={() => addSpecFun()}>Submit</Button>
            </Col>
          </Row>
          <Row>
            <Col xs={4} xl={3} className="content-item-title">
              Product Specification value:
            </Col>
            <Col xs={2} xl={2}>
              <Form.Control as="select" ref={specChoose}>
                {specList.map((spec, index) => {
                  return <option value={spec.id}>{spec.spec_name}</option>;
                })}
              </Form.Control>
            </Col>
            <Col xs={3} xl={3}>
              <Form.Control
                placeholder="like: 64GB, Space Grey, A+"
                ref={specValue}
              />
            </Col>
            <Col xs={3} xl={4} variant="warning">
              <Button onClick={() => addSpecValueFun()}>Submit</Button>
            </Col>
          </Row>
        </div>

        <Row>
          <Col xs={12} lg={7}>
            <div className="content-cardbox">
              <Table>
                <thead>
                  <tr>
                    <th>Specification ID</th>
                    <th>Specification</th>
                    <th>EDIT</th>
                  </tr>
                </thead>
                <tbody>
                {specList.map((val, index) => {
                  return (
                    <>
                      <tr key={index}>
                        {/* <td key={index}>{val.id}</td> */}
                        <EditSpec index={index} val={val} key={val.id} />
                        {/* <td><Button variant='danger' onClick={(e)=>deleteSpec(val.id)}>Delete</Button><Button variant='info' onClick={(e)=>Edit(id)}>Alter</Button></td> */}
                      </tr>
                    </>
                  );
                })}
                </tbody>
              </Table>
            </div>
          </Col>
          {/* ---------------------------specification value-------------------------------------- */}
          <Col xs={12} lg={5}>
            <div className="content-cardbox">
              <div className='content-item-title'>Choose Specification:</div>
              <Form.Control as="select" onChange={(e)=>setspecChooseShow(e.target.value)}>
                <option value="">---</option>
                {specList.map((spec, index) => {
                  return <option value={spec.id}>{spec.spec_name}</option>;
                })}
              </Form.Control>
              <Table>
                <thead>
                  <tr>
                    <th>Specification Value ID</th>
                    <th>Value</th>
                    <th>EDIT</th>
                  </tr>
                </thead>
                <tbody>
                  <EditSpecValue />
                </tbody>
              </Table>
            </div>
          </Col>
          {/* ---------------------------specification value END-------------------------------------- */}
        </Row>
      </Container>

      <Modal show={deleteShow} onHide={() => deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to <u>DELETE</u> <strong>{deleteSpec}{deleteSpecValue}</strong>? It will
          influence related SPU! You will need to re-edit related SKU.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => deleteSpecFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default ProductSpec;
