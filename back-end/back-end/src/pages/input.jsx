import React, { useState } from "react";
import { Col, Row, Container, Form, Button } from "react-bootstrap";
import Axios from "axios";

function ContentInput() {
  const [category, setCategory] = useState("");
  const [categoryList, setCategoryList] = useState([]);
  const [brand, setBrand] = useState("");
  const [spu, setSpu] = useState("");

  const addCategory = () => {
    Axios.post("http://chvast.com:3001/input", { category: category }).then(
      () => {
        console.log("success");
      }
    );
    console.log(category);
  };
  const addBrand = () => {
    Axios.post("http://chvast.com:3001/input", { brand: brand }).then(() => {
      console.log("success");
    });
    console.log(brand);
  };
  const addSpu = () => {
    Axios.post("http://chvast.com:3001/input", { spu: spu }).then(() => {
      console.log("success");
    });
    console.log(spu);
  };
  const getCategory = () => {
    Axios.get("http://chvast.com:3001/get").then((Response) => {
      setCategoryList(Response.data);
      console.log(Response.data);
    });
  };


  return (
    <Form>
      <Container>
        <Row>
          <Col>
            <Form.Group as={Col} controlId="formGridProductCategory">
              <Form.Label>product category</Form.Label>
              <Form.Control
                required
                placeholder="product category"
                onChange={(e) => {
                  setCategory(e.target.value);
                }}
              />
              <Button onClick={addCategory}>Add product category</Button>
            </Form.Group>
          </Col>
          <Col>
            <Button onClick={getCategory}>show category</Button>
            <div></div>
            <ul>
              {categoryList.map((val, key) => {
                return <li>{val.category_name}</li>;
              })}
            </ul>
          </Col>
        </Row>
        <Row>
          <Form.Group as={Col} controlId="formGridProductBrand">
            <Form.Label>product brand</Form.Label>
            <Form.Control
              required
              placeholder="product brand"
              onChange={(e) => {
                setBrand(e.target.value);
              }}
            />
            <Button onClick={addBrand}>Add product brand</Button>
          </Form.Group>
        </Row>
        <Row>
          <Form.Group as={Col} controlId="formGridSpu">
            <Form.Label>spu</Form.Label>
            <Form.Control
              required
              placeholder="product spu"
              onChange={(e) => {
                setSpu(e.target.value);
              }}
            />
            <Button onClick={addSpu}>Add spu</Button>
          </Form.Group>
        </Row>
      </Container>
    </Form>
  );
}
export default ContentInput;
