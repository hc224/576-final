import React, { useEffect, useState, useRef } from "react";

import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
  Modal,
} from "react-bootstrap";

import Axios from "axios";
import "../css/productSPU.css";

function ProductSPU() {
  const [SPU, setSPU] = useState([]);
  const [categories, setCategories] = useState([]);
  const [category, setCategory] = useState("");
  const [brands, setBrands] = useState([]);
  const [brand, setBrand] = useState("");
  const [deleteSPU, setdeleteSPU] = useState("");
  const [SPUA, setSPUA] = useState("");
  const newSPU = useRef(null);
  const SPUNo = useRef(null);
  const categoryID = useRef(null);
  const brandID = useRef(null);
  const spuNoChange = useRef(null);
  const categoryChange = useRef(null);
  const brandChange = useRef(null);
  const productChange = useRef(null);

  const [deleteShow, setdeleteShow] = useState(false);
  const deleteCloseHandle = () => setdeleteShow(false);
  const deleteShowHandle = () => setdeleteShow(true);
  useEffect(() => {
    getCategories();
    getBrands();

    console.log("use!!");
  }, []);

  useEffect(() => {
    getSPU();
  }, [category]);
  useEffect(() => {
    getSPU();
  }, [brand]);
  const getCategories = () => {
    Axios.get("http://chvast.com:3001/get-categories").then((Response) => {
      setCategories(Response.data);
    });
  };
  const getBrands = () => {
    Axios.get("http://chvast.com:3001/get-brand").then((Response) => {
      setBrands(Response.data);
    });
  };
  const getSPU = () => {
    Axios.post("http://chvast.com:3001/get-spu", {
      categoryID: category,
      brandID: brand,
    }).then((Response) => {
      setSPU(Response.data);
      console.log(Response.data);
    });
  };

  const deleteConfirm = (spuID) => {
    setdeleteSPU(spuID);
    console.log(spuID);
    deleteShowHandle();
  };
  const deleteSPUFun = () => {
    Axios.post("http://chvast.com:3001/delete-spu", {
      delSPU: deleteSPU,
    }).then(() => {
      getSPU();
      success(deleteSPU, "del");
      deleteCloseHandle();
    });
  };
  const alterSPUFun = (spuID) => {
    Axios.post("http://chvast.com:3001/alter-spu", {
      spuID: spuID,
      spuNo: spuNoChange.current.value,
      spuCategoryID: categoryChange.current.value,
      spuBrandID: brandChange.current.value,
      spuProduct: productChange.current.value,
    }).then(() => {
      success(productChange.current.value, "alt");
      // setCategory('');
      //setBrand('');
      setSPUA(-1); //return table (==cancel)

      getSPU();
    });
  };

  const addSPUFun = () => {
    Axios.post("http://chvast.com:3001/input-spu", {
      newSPU: newSPU.current.value,
      spuNo: SPUNo.current.value,
      categoryID: categoryID.current.value,
      brandID: brandID.current.value,
    }).then((Response) => {
      console.log("spu:");
      console.log(Response.data.sqlState);
      if (Response.data.sqlState === "23000") {
        alert("Fail to insert. Already has this SPU.");
      } else {
        alert("success!");
        getSPU();
      }
    });
  };
  const getSPUFun = () => {
    Axios.post("http://chvast.com:3001/get-spu").then((Response) => {
      setSPU(Response.data);
      console.log(Response.data);
    });
  };

  const success = (content, act) => {
    switch (act) {
      case "add":
        alert("Success! \n" + "Added new SPU: " + content);
        break;
      case "del":
        alert("Success! \n" + "Deleted SPU: " + content);
        break;
      case "alt":
        alert("Success! \n" + "Altered SPU: " + content);
        break;
    }
  };

  const Edit = () => {
    return SPU.map((spu, index) => {
      return (
        <>
          {SPUA === spu.id ? (
            <tr key={spu.id}>
              <td>
                <Form.Control
                  defaultValue={spu.sku_no}
                  ref={spuNoChange}
                ></Form.Control>
              </td>
              <td>
                <Form.Control
                  as="select"
                  size="sm"
                  ref={categoryChange}
                  defaultValue={categories.category_name}
                >
                  <option value="">---</option>
                  {categories.map((categories, index) => {
                    return (
                      <option value={categories.id} key={categories.id}>
                        {categories.category_name}
                      </option>
                    );
                  })}
                </Form.Control>
              </td>
              <td>
                <Form.Control as="select" size="sm" ref={brandChange}>
                  <option value="">---</option>
                  {brands.map((brands, index) => {
                    return (
                      <option value={brands.id} key={brands.id}>
                        {brands.brand_name}
                      </option>
                    );
                  })}
                </Form.Control>
              </td>
              <td>
                <Form.Control
                  defaultValue={spu.product_name}
                  ref={productChange}
                ></Form.Control>
              </td>
              <td>
                <Button
                  variant="danger"
                  className="edit-btn"
                  onClick={() => setSPUA(-1)}
                >
                  Cancel
                </Button>
                <Button
                  className="edit-btn"
                  onClick={() => alterSPUFun(spu.id)}
                >
                  Confirm
                </Button>
              </td>
            </tr>
          ) : (
            <tr key={spu.id + 100}>
              <td>{spu.spu_no}</td>
              <td>{spu.category_name}</td>
              <td>{spu.brand_name}</td>
              <td>{spu.product_name}</td>
              <td>
                <Button
                  variant="danger"
                  className="edit-btn"
                  onClick={() => deleteConfirm(spu.id)}
                >
                  Delete
                </Button>
                <Button
                  variant="info"
                  className="edit-btn"
                  //   onClick={() => setSPUA(spu.id)}
                  onClick={() => setSPUA(spu.id)}
                >
                  Alter
                </Button>
              </td>
            </tr>
          )}
        </>
      );
    });
  };
  // -------------------------------------main render----------------------------------------------
  return (
    <>
      <Container className="content-page" fluid>
        <div className="content-title">Product SPU</div>
        <div className="content-cardbox">
          <Row>
            <Col xs={2}>
              <span>SPU No.:</span>
              <Form.Control type="text" ref={SPUNo} size="sm"></Form.Control>
            </Col>
            <Col xs={3}>
              <span>Category:</span>
              <Form.Control as="select" size="sm" ref={categoryID}>
                {categories.map((categories, index) => {
                  return (
                    <option value={categories.id} key={categories.id}>
                      {categories.category_name}
                    </option>
                  );
                })}
              </Form.Control>
            </Col>
            <Col xs={3}>
              <span>Brand:</span>
              <Form.Control as="select" size="sm" ref={brandID}>
                {brands.map((brands, index) => {
                  return (
                    <option value={brands.id} key={brands.id}>
                      {brands.brand_name}
                    </option>
                  );
                })}
              </Form.Control>
            </Col>
            <Col xs={4}>
              <span>Product:</span>
              <Form.Control type="text" ref={newSPU} size="sm"></Form.Control>
            </Col>
            <Col xs={12} variant="warning">
              <Button onClick={() => addSPUFun()} className="add-spu-btn">
                Add SPU
              </Button>
            </Col>
          </Row>

          {/* ----------------------------->>>>>---SPU table start---<<<<<<<------------------------------------ */}
        </div>
        <div className="content-cardbox">
          <Row>
            <Col xs="6">
              <span>Category:</span>
              <Form.Control
                as="select"
                size="sm"
                onChange={(e) => setCategory(e.target.value)}
              >
                <option value="">---</option>
                {categories.map((categories, index) => {
                  return (
                    <option value={categories.id} key={categories.id}>
                      {categories.category_name}
                    </option>
                  );
                })}
              </Form.Control>
            </Col>
            <Col xs="6">
              <span>Brand:</span>
              <Form.Control
                as="select"
                size="sm"
                onChange={(e) => setBrand(e.target.value)}
              >
                <option value="">---</option>
                {brands.map((brands, index) => {
                  return (
                    <option value={brands.id} key={brand.id}>
                      {brands.brand_name}
                    </option>
                  );
                })}
              </Form.Control>
            </Col>
            <Col>
              <Button onClick={() => getSPUFun()} className="add-spu-btn">
                Query All
              </Button>
            </Col>
            <Col xs="12">
              <Table>
                <thead>
                  <tr>
                    <th>SPU ID</th>
                    <th>Category</th>
                    <th>Brand</th>
                    <th>Product</th>
                    <th>Edit</th>
                  </tr>
                </thead>
                <tbody>
                  <Edit />
                </tbody>
              </Table>
            </Col>
          </Row>
        </div>
      </Container>

      <Modal show={deleteShow} onHide={() => deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to <u>DELETE</u> <strong>{deleteSPU}</strong>? It will
          influence related SKU! You will need to re-edit related SKU.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => deleteSPUFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default ProductSPU;
