import React, { useEffect, useState } from "react";

import { Table, Button, Container, Modal, Alert } from "react-bootstrap";
import Axios from "axios";

function UserOrder() {
  const [userOrder, setuserOrder] = useState([]);
  const [userOrderDetail, setuserOrderDetail] = useState([]);
  const [orderNo, setorderNo] = useState(-1);

  const [deleteShow, setdeleteShow] = useState(false);
  const deleteCloseHandle = () => setdeleteShow(false);
  const deleteShowHandle = () => setdeleteShow(true);

  const [viewShow, setviewShow] = useState(false);
  const viewCloseHandle = () => setviewShow(false);
  const viewShowHandle = () => setviewShow(true);

  const getuserOrder = () => {
    Axios.get("http://chvast.com:3001/get-user-order").then((Response) => {
      setuserOrder(Response.data);
    });
  };
  const getuserOrderDetail = (orderNo) => {
    setorderNo(orderNo);
    console.log(orderNo);
    Axios.post("http://chvast.com:3001/get-user-order-detail", {
      orderNo: orderNo,
    }).then((Response) => {
      console.log(Response.data);
      setuserOrderDetail(Response.data);
      viewShowHandle();
    });
  };
  const deleteorderFun = () => {
    Axios.post("http://chvast.com:3001/delete-order", {
      orderNo: orderNo,
    }).then((Response) => {
      alert("successfully delete order.");
      viewCloseHandle();
      deleteCloseHandle();
      getuserOrder();
    });
  };

  useEffect(() => {
    getuserOrder();
  }, []);
  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">User Orders:</div>
        <div className="content-cardbox">
          <Table>
            <thead>
              <tr>
                <th>Order</th>
                <th>Date</th>
                <th>User Name</th>
                <th>Order Status</th>
                <th>Subtotal</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>
              {userOrder.map((order, index) => {
                return (
                  <tr className="link-style">
                    <td>{order.order_no}</td>
                    <td>{order.order_time.replace(/T/,' ').slice(0,-5)}</td>

                    <td>
                      {order.first_name} {order.last_name}
                    </td>
                    <td>
                      <Alert
                        variant={
                          order.order_status === "Paid" ? "primary" : (order.order_status==='Processing' ? 'success' : 'warning')
                        }
                        className="alert-box"
                      >
                        {order.order_status}
                      </Alert>
                    </td>
                    <td>{order.subtotal}</td>
                    <td>
                      <Button
                        variant="success"
                        className="edit-btn"
                        onClick={() => getuserOrderDetail(order.order_no)}
                      >
                        View
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>
      </Container>
      <Modal show={deleteShow} onHide={() => deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to <u>DELETE</u> <strong>{orderNo}</strong>?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => deleteorderFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={viewShow} onHide={() => viewCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>User Order View:</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {userOrderDetail.map((d, i) => {
            return (
              <div className="view-order-detail">
                <div>order Number: {orderNo}</div>
                <div>
                  {d.first_name} {d.last_name}
                </div>
                <div>-----------------</div>
                <div>{d.address1} </div>
                <div>{d.address2} </div>
                <div>
                  {d.city} {d.zip}
                </div>
                <div>--------------------------------</div>
                <div>Order Status: {d.order_status}</div>
                <div>Payment: {d.payment}</div>
              </div>
            );
          })}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => viewCloseHandle()}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => deleteShowHandle()}>
            Delect
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default UserOrder;
