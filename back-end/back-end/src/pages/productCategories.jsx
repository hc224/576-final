import React, { useEffect, useState, useRef } from "react";

import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
  Modal,
} from "react-bootstrap";

import Axios from "axios";

function ProductCategories() {
  const [categoriesList, setcategoriesList] = useState([]);
  const [categoryA, setcategoryA] = useState(0);
  const [deleteCategory, setdeleteCategory] = useState("");
  const categoryChange = useRef(null);
  const category = useRef(null);

  const [deleteShow, setdeleteShow] = useState(false);
  const deleteCloseHandle = () => setdeleteShow(false);
  const deleteShowHandle = () => setdeleteShow(true);

  const addCategoryFun = () => {
    Axios.post("http://chvast.com:3001/input-categories", {
      // categories: addCategory,
      categories: category.current.value,
    }).then(() => {
      getcategories();
      success(category.current.value, "add");
    });
  };
  const deleteConfirm = (categoryName) => {
    setdeleteCategory(categoryName);
    console.log(categoryName);
    deleteShowHandle();
  };
  const deleteCategoryFun = () => {
    Axios.post("http://chvast.com:3001/delete-categories", {
      delCate: deleteCategory,
    }).then(() => {
      getcategories();
      success(deleteCategory, "del");
      deleteCloseHandle();
    });
  };
  const alterCategory = (alCateId) => {
    Axios.post("http://chvast.com:3001/alter-categories", {
      categories: categoryChange.current.value,
      alCateId: alCateId,
    }).then(() => {
      getcategories();
      console.log("success");
    });
    success(categoryChange.current.value, "alt");
    setcategoryA(0);
  };
  const getcategories = () => {
    Axios.get("http://chvast.com:3001/get-categories").then((Response) => {
      setcategoriesList(Response.data);
      console.log(Response.data);
    });
  };

  useEffect(() => {
    getcategories();
    console.log("useEffect!!!");
  }, [categoriesList.id]);

  const success = (content, act) => {
    switch (act) {
      case "add":
        alert("Success! \n" + "Added new category: " + content);
        break;
      case "del":
        alert("Success! \n" + "Deleted category: " + content);
        break;
      case "alt":
        alert("Success! \n" + "Altered category: " + content);
        break;
    }
  };

  const Edit = (props) => {
    return (
      <>
        <td>{props.val.id}</td>
        {categoryA === props.val.id ? (
          <>
            <td>
              <Form.Control
                type="text"
                placeholder={props.val.category_name}
                ref={categoryChange}
              />
              {/* must use uncontrolled input cause controlled input will re-render when input something and conditional rendering will re-render input */}
            </td>
            <td>
              <Button type="submit" onClick={() => alterCategory(props.val.id)}>
                Confirm
              </Button>
              <Button
                variant="danger"
                type="submit"
                onClick={() => {
                  setcategoryA(-1);
                }}
                className="edit-btn"
              >
                Cancel
              </Button>
            </td>
          </>
        ) : (
          <>
            <td>{props.val.category_name}</td>
            <td>
              <Button
                variant="danger"
                onClick={() => deleteConfirm(props.val.category_name)}
                className="edit-btn"
              >
                Delete
              </Button>
              <Button
                variant="info"
                onClick={() => setcategoryA(props.val.id)}
                className="edit-btn"
              >
                Alter
              </Button>
            </td>
          </>
        )}
      </>
    );
  };

  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">Product categories</div>

        <div className="content-cardbox">
          <Row>
            <Col xs={4} xl={3} className="content-item-title">
              Product categories:
            </Col>
            <Col xs={5} xl={5}>
              <Form.Control
                placeholder="product categories"
                ref={category}
                // onChange={(e) => {
                //    setaddCategory(e.target.value);
                //   console.log(e.target.value);
                // }}
              />
            </Col>
            <Col xs={3} xl={4} variant="warning">
              <Button onClick={() => addCategoryFun()}>Submit</Button>
            </Col>
          </Row>
        </div>

        <div className="content-cardbox">
          <Table>
            <thead>
              <tr>
                <th>Category ID</th>
                <th>categories</th>
                <th>EDIT</th>
              </tr>
            </thead>
            {categoriesList.map((val, index) => {
              return (
                <>
                  <tr key={index}>
                    {/* <td key={index}>{val.id}</td> */}
                    <Edit index={index} val={val} key={val.id} />
                    {/* <td><Button variant='danger' onClick={(e)=>deleteCategory(val.id)}>Delete</Button><Button variant='info' onClick={(e)=>Edit(id)}>Alter</Button></td> */}
                  </tr>
                </>
              );
            })}
          </Table>
        </div>
      </Container>

      <Modal show={deleteShow} onHide={() => deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to <u>DELETE</u> <strong>{deleteCategory}</strong>? It
          will influence related SPU! You will need to re-edit related SPU.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => deleteCategoryFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default ProductCategories;
