import React, { useEffect, useState,useRef } from "react";

import { Table, Button, Container } from "react-bootstrap";

import Axios from "axios";

function ProductDetail(props) {
  const [SKU, setSKU] = useState([]);
  const [productDesc, setproductDesc] = useState("");
  // const [description, setdescription] = useState("");
  const description=useRef(null);

  const getProductInfo = () => {
    Axios.post("http://chvast.com:3001/get-sku", {
      sku: props.location.state.sku,
    }).then((Response) => {
      setSKU(Response.data);
    });
  };

  const getProducDesc = () => {
    Axios.post("http://chvast.com:3001/get-product-desc", {
      skuID: props.location.state.sku,
    }).then((Response) => {
      if (Response.data.length !== 0) {
        setproductDesc(Response.data[0].description);
      }

      console.log(Response.data);
    });
  };
  const saveDesc = () => {
    Axios.post("http://chvast.com:3001/input-desc", {
      description: description.current.value,
      sku: props.location.state.sku,
    }).then((Response) => {
      if (Response.data === "success") {
        alert("success.");
      } else {
        alert("Wrong!");
      }
    });
  };

  useEffect(() => {
    getProducDesc();
    getProductInfo();
  }, []);

  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">Product Detail:</div>

        <div className="content-cardbox">
          <Table>
            <thead>
              <tr>
                <th>SKU No.</th>
                <th>SPU</th>
                <th>Color</th>
                <th>Storage</th>
                <th>Grade</th>
                <th>Regular Price</th>
                <th>Sale Price</th>
                <th>Stock</th>
              </tr>
            </thead>
            {
              <tbody>
                <tr>
                  {SKU.map((sku, index) => {
                    return (
                      <>
                        <td>{sku.sku_no}</td>
                        <td>{sku.product_name}</td>
                        <td>{sku.color}</td>
                        <td>{sku.storages}</td>
                        <td>{sku.grade}</td>
                        <td>{sku.regular_price}</td>
                        <td>{sku.sale_price}</td>
                        <td>{sku.stock}</td>
                      </>
                    );
                  })}
                </tr>
              </tbody>
            }
          </Table>
        </div>
        <div className="content-cardbox">
          {/* <textarea name='pro-desc' type='text'  cols="180" rows="120" className='text-area' Value={productDesc} ref={description}></textarea> */}
          <textarea
            name="pro-desc"
            type="text"
            cols="50"
            rows="80"
            className="text-area"
            defaultValue={productDesc}
            // onChange={(e) => setdescription(e.target.value)}
            ref={description}
          ></textarea>
          {/* <div
  className="fixedTextArea"
  contentEditable
  suppressContentEditableWarning ref={description}>
{productDesc}
</div> */}
        </div>
        <Button className="edit-btn col-h-xs-10" onClick={() => saveDesc()}>
          Save
        </Button>
      </Container>
    </>
  );
}
export default ProductDetail;
