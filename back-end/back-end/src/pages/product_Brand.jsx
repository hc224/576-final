import React, { useEffect, useState, useRef } from "react";

import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
  Modal
} from "react-bootstrap";

import Axios from "axios";

function ProductBrand() {
  const [brandList, setbrandList] = useState([]);
  const [brandA, setbrandA] = useState(0);
  const [deleteBrand, setdeleteBrand] = useState('');
const brandChange=useRef(null);
const brand=useRef(null);

const [deleteShow, setdeleteShow] = useState(false);
const deleteCloseHandle = () => setdeleteShow(false);
const deleteShowHandle = () => setdeleteShow(true);

  const addBrandFun = () => {
    Axios.post("http://chvast.com:3001/input-brand", {
      brand:brand.current.value
    }).then(() => {
      getbrand();
      success(brand.current.value,'add');
    });
   
  };
  const deleteConfirm=(brandName)=>{
      setdeleteBrand(brandName);
      console.log(brandName);
    deleteShowHandle();
  }
  const deleteBrandFun = () => {
    Axios.post("http://chvast.com:3001/delete-brand", {
        delBrand: deleteBrand,
    }).then(() => {
      getbrand();
      success(deleteBrand,'del');
deleteCloseHandle();
    });

  };
  const alterBrand = (alBrandId) => {
    Axios.post("http://chvast.com:3001/alter-brand", {
      brand: brandChange.current.value,
      alBrandId: alBrandId,
    }).then(() => {
      getbrand();
      console.log("success");
    });
    success(brandChange.current.value,'alt');
    setbrandA(0);
  };
  const getbrand = () => {
    Axios.get("http://chvast.com:3001/get-brand").then((Response) => {
      setbrandList(Response.data);
      console.log(Response.data);
    });
  };
  
  useEffect(() => {
    getbrand();
    console.log('useEffect!!!');
  }, [brandList.id]);


  const success = (content,act) => {
switch (act){
case 'add':
  alert("Success! \n"+'Added new Brand: '+content);
break;
case 'del':
  alert("Success! \n"+'Deleted Brand: '+content);
  break;
  case 'alt':
    alert("Success! \n"+'Altered Brand: '+content);
    break;
}
  };





  const Edit = (props) => {
    return (
      <>
        <td>{props.val.id}</td>
        {brandA === props.val.id ? (
          <>
            <td>
              <Form.Control
                type="text"
                placeholder={props.val.brand_name}
                ref={brandChange}
              /> 
              {/* must use uncontrolled input cause controlled input will re-render when input something and conditional rendering will re-render input */}
              </td>
              <td>
              <Button type="submit" onClick={() => alterBrand(props.val.id)} className='edit-btn'>
                Confirm
              </Button>
              <Button variant='danger' type="submit" onClick={() => {setbrandA(-1)}} className='edit-btn'>
                Cancel
              </Button>
            </td>
          </>
        ) : (
          <>
            <td>{props.val.brand_name}</td>
            <td>
              <Button
                variant="danger"
                onClick={() => deleteConfirm(props.val.brand_name)} className='edit-btn'
              >
                Delete
              </Button>
              <Button
                variant="info"
                onClick={() => setbrandA(props.val.id)} className='edit-btn'
              >
                Alter
              </Button>
            </td>
          </>
        )}
      </>
    );
  };

  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">Product brand</div>
        
          <div className="content-cardbox">
            <Row>
              <Col xs={4} xl={3} className="content-item-title">
                Product brand:
              </Col>
              <Col xs={5} xl={5}>
                <Form.Control
                  placeholder="product brand"
                  ref={brand}
                  // onChange={(e) => {
                  //    setaddBrand(e.target.value);
                  //   console.log(e.target.value);
                  // }}
                />
              </Col>
              <Col xs={3} xl={4} variant="warning">
                <Button onClick={()=>addBrandFun()}>Submit</Button>
              </Col>
            </Row>
          </div>
        
        <div className="content-cardbox">
          <Table>
            <thead>
              <tr>
                <th>Brand ID</th>
                <th>brand</th>
                <th>EDIT</th>
              </tr>
              </thead>
              {brandList.map((val, index) => {
                return (
                  <>
                    <tr key={index}>
                      {/* <td key={index}>{val.id}</td> */}
                      <Edit index={index} val={val} key={val.id} />
                      {/* <td><Button variant='danger' onClick={(e)=>deleteBrand(val.id)}>Delete</Button><Button variant='info' onClick={(e)=>Edit(id)}>Alter</Button></td> */}
                    </tr>
                  </>
                );
              })}
            
          </Table>
        </div>
      </Container>

      
<Modal show={deleteShow} onHide={()=>deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure to <u>DELETE</u> <strong>{deleteBrand}</strong>? It will influence related SPU! You will need to re-edit related SPU.</Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={()=>deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={()=>deleteBrandFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default ProductBrand;
