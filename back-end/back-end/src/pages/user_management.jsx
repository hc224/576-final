import React, { useEffect, useState } from "react";
import { Table, Button, Container, Modal } from "react-bootstrap";
import Axios from "axios";

function UserManagement() {
  const [userList, setuserList] = useState([]);
  const [deleteUser, setdeleteUser] = useState([]);
  const [shippingUserID, setshippingUserID] = useState(-1);
  const [userShipping, setuserShipping] = useState([]);

  const [deleteShow, setdeleteShow] = useState(false);
  const deleteCloseHandle = () => setdeleteShow(false);
  const deleteShowHandle = () => setdeleteShow(true);
  const [shippingShow, setshippingShow] = useState(false);
  const shippingCloseHandle = () => setshippingShow(false);
  const shippingShowHandle = () => setshippingShow(true);

  const getUser = () => {
    Axios.get("http://chvast.com:3001/get-user").then((Response) => {
      setuserList(Response.data);
    });
  };
  const getUserShipping = () => {
    Axios.post("http://chvast.com:3001/get-user-shipping", {
      userID: shippingUserID,
    }).then((Response) => {
      setuserShipping(Response.data);
    });
  };

  const deleteConfirm = (userID) => {
    setdeleteUser(userID);
    console.log(userID);
    deleteShowHandle();
  };
  const userShippingFun = (userID) => {
    setshippingUserID(userID);
    getUserShipping();
    console.log(userID);
    shippingShowHandle();
  };

  const deleteUserFun = () => {
    Axios.post("http://chvast.com:3001/delete-user", {
      deluser: deleteUser,
    }).then(() => {
      getUser();
      alert("delete successfully.");
      deleteCloseHandle();
    });
  };

  useEffect(() => {
    getUser();
  }, []);
  return (
    <>
      <Container fluid className="content-page">
        <div className="content-title">User Management</div>
        <div className="content-cardbox">
          <Table>
            <thead>
              <tr>
                <th>User ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>registration date</th>
                <th>EDIT</th>
              </tr>
            </thead>
            <tbody>
              {userList.map((user, index) => {
                return (
                  <tr className="link-style">
                    <td>{user.id}</td>
                    <td>{user.first_name}</td>
                    <td>{user.last_name}</td>
                    <td>{user.email}</td>
                    <td>{user.date_register}</td>
                    <td>
                      <Button
                        variant="success"
                        onClick={() => userShippingFun(user.id)}
                        className="edit-btn"
                      >
                        Shipping
                      </Button>
                      <Button
                        variant="danger"
                        onClick={() => deleteConfirm(user.id)}
                        className="edit-btn"
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>
      </Container>

      <Modal show={deleteShow} onHide={() => deleteCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to <u>DELETE</u> <strong>{deleteUser}</strong>? It will
          influence related SPU! You will need to re-edit related SPU.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => deleteCloseHandle()}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => deleteUserFun()}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={shippingShow} onHide={() => shippingCloseHandle()}>
        <Modal.Header closeButton>
          <Modal.Title>Shipping Info.</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table>
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>address 1</th>
                <th>address 2</th>
                <th>City</th>
                <th>postcode</th>
              </tr>
            </thead>
            <tbody>
              {userShipping.map((shipping, index) => {
                return (
                  <tr className="link-style">
                    <td>{shipping.first_name}</td>
                    <td>{shipping.last_name}</td>
                    <td>{shipping.address1}</td>
                    <td>{shipping.address2}</td>
                    <td>{shipping.city}</td>
                    <td>{shipping.postcode}</td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => shippingCloseHandle()}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default UserManagement;
